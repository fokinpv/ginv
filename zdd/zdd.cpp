/***************************************************************************
 *   Copyright (C) 2011 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "zdd.h"

namespace GInv {

ZDD::Cache::~Cache() {
  RBTree<Chain*, compare>::ConstIterator i(mCache.minimum());
  while(i) {
    Chain::ConstIterator j(i.key()->begin());
    while(j) {
      mAllocator->destroy((ZDD::Node*)j.data());
      ++j;
    }
    mAllocator->destroy(i.key());
    ++i;
  }
}

void ZDD::Cache::swap(Cache &a) {
  Allocator* tmp1=mAllocator;
  mAllocator = a.mAllocator;
  a.mAllocator = tmp1;

  mCache.swap(a.mCache);

  size_t tmp2=mCollision;
  mCollision = a.mCollision;
  a.mCollision = tmp2;

  size_t tmp3=mCount;
  mCount = a.mCount;
  a.mCount = tmp3;

  mTmp1.swap(a.mTmp1);
  mTmp1.begin().insert(&mTmp2);
  a.mTmp1.begin().insert(&a.mTmp2);
}

const ZDD::Node* ZDD::Cache::createNode(Variable v, const ZDD::Node* m, const ZDD::Node* a) {
  assert(m != ZDD::sZero);
  const Node* r=NULL;
  mTmp2.mVariable = v;
  mTmp2.mMult = m;
  mTmp2.mAdd = a;
  RBTree<Chain*, compare>::ConstIterator f=mCache.find(&mTmp1);
  if (!f) {
    r = new(mAllocator) Node(v, m, a);
    ++mCount;
    Chain *c=new(mAllocator) Chain(mAllocator);
    c->begin().insert(r);
    mCache.insert(c);
  }
  else {
    Chain::ConstIterator i(f.key()->begin());
    while(i) {
      const Node* n=i.data();
      assert(n->mVariable == v &&
             n->mMult->mVariable == m->mVariable &&
             n->mAdd->mVariable == a->mVariable);
      if (n->mVariable == v && n->mMult == m && n->mAdd == a)
        break;
      ++i;
    }
    if (i) {
      r = i.data();
    }
    else {
      r = new(mAllocator) Node(v, m, a);
      ++mCount;
      f.key()->begin().insert(r);
      if (f.key()->length() > mCollision) {
        ++mCollision;
        assert(mCollision == f.key()->length());
      }
    }
  }
  return r;
}

#ifdef GINV_ZDD_GRAPHVIZ
void ZDD::Cache::saveImage(const char* format, const char* filename) const {
  char buffer[256];
  GVC_t *gvc=gvContext();
  Agraph_t *g=agopen((char*)"ZDD::Cache", AGDIGRAPH);
  for(RBTree<Chain*, compare>::ConstIterator i(mCache.minimum()); i; ++i) {
    for(Chain::ConstIterator j(i.key()->begin()); j; ++j) {
      sprintf(buffer, "%p", j.data());
      Agnode_t *r=agnode(g, buffer);
      sprintf(buffer, "%d", j.data()->mVariable);
      agsafeset(r, (char*)"label", buffer, (char*)"");

      Agnode_t *m;
      if (j.data()->mMult->mVariable >= 0) {
        sprintf(buffer, "%p", j.data()->mMult);
        m = agnode(g, buffer);
        sprintf(buffer, "%d", j.data()->mMult->mVariable);
        agsafeset(m, (char*)"label", buffer, (char*)"");
      }
      else {
        assert(j.data()->mMult == ZDD::sOne);
        m = agnode(g, (char*)"1");
        agsafeset(m, (char*)"shape", (char*)"box", (char*)"");
      }

      Agnode_t *a;
      if (j.data()->mAdd->mVariable >= 0) {
        sprintf(buffer, "%p", j.data()->mAdd);
        a = agnode(g, buffer);
        sprintf(buffer, "%d", j.data()->mAdd->mVariable);
        agsafeset(a, (char*)"label", (char*)buffer, (char*)"");
      }
      else {
        if (j.data()->mAdd == ZDD::sZero)
          a = agnode(g, (char*)"0");
        else {
          assert(j.data()->mAdd == ZDD::sOne);
          a = agnode(g, (char*)"1");
        }
        agsafeset(a, (char*)"shape", (char*)"box", (char*)"");
      }

      Agedge_t *e=agedge(g, r, m);
      agsafeset(e, (char*)"style", (char*)"solid", (char*)"");
      agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
      e = agedge(g, r, a);
      agsafeset(e, (char*)"style", (char*)"dashed", (char*)"");
      agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
    }
  }
  gvLayout(gvc, g, (char*)"dot");
  gvRenderFilename(gvc, g, format, filename);
  gvFreeLayout(gvc, g);
  agclose(g);
}
#endif // GINV_ZDD_GRAPHVIZ

const ZDD::Node *const ZDD::sZero=new ZDD::Node;
const ZDD::Node *const ZDD::sOne=new ZDD::Node;

const ZDD::Node* ZDD::copy(ZDD::Cache* cache, const ZDD::Node *j) {
  const Node* r;
  if (j->mVariable < 0)
    r = j;
  else
    r = cache->createNode(j->mVariable,
                          copy(cache, j->mMult),
                          copy(cache, j->mAdd));
  return r;
}

const ZDD::Node* ZDD::addLocal(ZDD::Cache* cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i->mVariable > j->mVariable)
    r = cache->createNode(i->mVariable,
                          copy(cache, i->mMult),
                          addLocal(cache, i->mAdd, j));
  else if (i->mVariable < j->mVariable)
    r = cache->createNode(j->mVariable,
                          copy(cache, j->mMult),
                          addLocal(cache, i, j->mAdd));
  else if (i->mVariable < 0) {
    assert(i == sOne || i == sZero);
    assert(j == sOne || j == sZero);
    if (i == j)
      r = sZero;
    else
      r = sOne;
  }
  else {
    const Node *m=addLocal(cache, i->mMult, j->mMult),
               *a=addLocal(cache, i->mAdd, j->mAdd);
    if (m == sZero)
      r = a;
    else
      r = cache->createNode(i->mVariable, m, a);
  }
  return r;
}

const ZDD::Node* ZDD::addShared(ZDD::Cache* cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i->mVariable > j->mVariable)
    r = cache->createNode(i->mVariable,
                          i->mMult,
                          addShared(cache, i->mAdd, j));
  else if (i->mVariable < j->mVariable)
    r = cache->createNode(j->mVariable,
                          j->mMult,
                          addShared(cache, i, j->mAdd));
  else if (i->mVariable < 0) {
    assert(i == sOne || i == sZero);
    assert(j == sOne || j == sZero);
    if (i == j)
      r = sZero;
    else
      r = sOne;
  }
  else {
    const Node *a=addShared(cache, i->mAdd, j->mAdd);
    if (i->mMult == j->mMult)
      r = a;
    else
      r = cache->createNode(i->mVariable, addShared(cache, i->mMult, j->mMult), a);
  }
  return r;
}

const ZDD::Node* ZDD::multLocal(ZDD::Cache* cache, const ZDD::Node *i, ZDD::Variable v) {
  assert(v >= 0);
  const Node *r;
  if (i->mVariable > v) {
    const Node *m=multLocal(cache, i->mMult, v);
    if (m == sZero)
      r = multLocal(cache, i->mAdd, v);
    else
      r = cache->createNode(i->mVariable, m, multLocal(cache, i->mAdd, v));
  }
  else if (i->mVariable == v) {
    if (i->mMult == i->mAdd)
      r = sZero;
    else
      r = cache->createNode(v, addLocal(cache, i->mMult, i->mAdd), sZero);
  }
  else if (i->mVariable >= 0)
    r = cache->createNode(v, copy(cache, i), sZero);
  else {
    if (i == sZero)
      r = sZero;
    else
      r = cache->createNode(v, sOne, sZero);
  }
  return r;
}

const ZDD::Node* ZDD::multShared(ZDD::Cache* cache, const ZDD::Node *i, ZDD::Variable v) {
  assert(v >= 0);
  const Node *r;
  if (i->mVariable > v) {
    const Node *m=multShared(cache, i->mMult, v);
    if (m == sZero)
      r = multShared(cache, i->mAdd, v);
    else
      r = cache->createNode(i->mVariable, m, multShared(cache, i->mAdd, v));
  }
  else if (i->mVariable == v) {
    if (i->mMult == i->mAdd)
      r = sZero;
    else
      r = cache->createNode(v, addShared(cache, i->mMult, i->mAdd), sZero);
  }
  else if (i->mVariable >= 0)
    r = cache->createNode(v, i, sZero);
  else {
    if (i == sZero)
      r = sZero;
    else
      r = cache->createNode(v, sOne, sZero);
  }
  return r;
}

const ZDD::Node* ZDD::multLocal(ZDD::Cache* cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i == sOne)
    r = j;
  else if (i == sZero || j == sZero)
    r = sZero;
  else if (j == sOne || i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            multLocal(cache, i->mMult, j),
                            multLocal(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = cache->createNode(j->mVariable,
                            multLocal(cache, j->mMult, i),
                            multLocal(cache, j->mAdd, i));
    else {
      //x(ac+ad+cb)+bd
      const Node* ac = multLocal(cache, i->mAdd, j->mMult);
      const Node* ad = multLocal(cache, i->mAdd, j->mAdd);
      const Node* cb = multLocal(cache, j->mMult, i->mMult);
      const Node* sum = addLocal(cache, ac, addLocal(cache, ad, cb));
      if (sum == sZero)
        r = sZero;
      else
        r = cache->createNode((i->mVariable > j->mVariable) ? i->mVariable : j->mVariable,
                              sum,
                              multLocal(cache, i->mAdd, j->mAdd));
    }
  }
  return r;
}

const ZDD::Node* ZDD::multShared(ZDD::Cache* cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i == sOne)
    r = j;
  else if (i == sZero || j == sZero)
    r = sZero;
  else if (j == sOne || i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            multShared(cache, i->mMult, j),
                            multShared(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = cache->createNode(j->mVariable,
                            multShared(cache, j->mMult, i),
                            multShared(cache, j->mAdd, i));
    else {
      //x(ac+ad+cb)+bd
      const Node* ac = multShared(cache, i->mAdd, j->mMult);
      const Node* ad = multShared(cache, i->mAdd, j->mAdd);
      const Node* cb = multShared(cache, j->mMult, i->mMult);
      const Node* sum = addLocal(cache, ac, addLocal(cache, ad, cb));
      if (sum == sZero)
        r = sZero;
      else
        r = cache->createNode((i->mVariable > j->mVariable) ? i->mVariable : j->mVariable,
                              sum,
                              multShared(cache, i->mAdd, j->mAdd));
    }
  }
  return r;
}

const ZDD::Node* ZDD::unionLocal(Cache *cache, const Node *i, const Node *j) {
  const Node *r;
  if (i == sZero)
    r = j;
  else if (j == sZero)
    r = i;
  else if (i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            copy(cache, i->mMult),
                            unionLocal(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = cache->createNode(j->mVariable,
                            copy(cache, j->mMult),
                            unionLocal(cache, i, j->mAdd));
    else {
      const Node *m=unionLocal(cache, i->mMult, j->mMult),
                 *a=unionLocal(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

const ZDD::Node* ZDD::unionShared(Cache *cache, const Node *i, const Node *j) {
  const Node *r;
  if (i == sZero)
    r = j;
  else if (j == sZero)
    r = i;
  else if (i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            i->mMult,
                            unionShared(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = cache->createNode(j->mVariable,
                            j->mMult,
                            unionShared(cache, i, j->mAdd));
    else {
      const Node *m=unionShared(cache, i->mMult, j->mMult),
                 *a=unionShared(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

const ZDD::Node* ZDD::intersectionLocal(Cache *cache, const Node *i, const Node *j) {
  const Node *r;
  if (i == sZero)
    r = sZero;
  else if (j == sZero)
    r = sZero;
  else if (i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = intersectionLocal(cache, i->mAdd, j);
    else if (i->mVariable < j->mVariable)
      r = intersectionLocal(cache, i, j->mAdd);
    else {
      const Node *m=intersectionLocal(cache, i->mMult, j->mMult),
                 *a=intersectionLocal(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

const ZDD::Node* ZDD::intersectionShared(Cache *cache, const Node *i, const Node *j) {
  const Node *r;
  if (i == sZero)
    r = sZero;
  else if (j == sZero)
    r = sZero;
  else if (i == j)
    r = i;
  else {
    if (i->mVariable > j->mVariable)
      r = intersectionShared(cache, i->mAdd, j);
    else if (i->mVariable < j->mVariable)
      r = intersectionShared(cache, i, j->mAdd);
    else {
      const Node *m=intersectionShared(cache, i->mMult, j->mMult),
                 *a=intersectionShared(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

const ZDD::Node* ZDD::diffLocal(Cache *cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i == sZero)
    r = sZero;
  else if (j == sZero)
    r = i;
  else if (i == j)
    r = sZero;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            copy(cache, i->mMult),
                            diffLocal(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = diffLocal(cache, i, j->mAdd);
    else {
      const Node *m=diffLocal(cache, i->mMult, j->mMult),
                 *a=diffLocal(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

const ZDD::Node* ZDD::diffShared(Cache *cache, const ZDD::Node *i, const ZDD::Node *j) {
  const Node *r;
  if (i == sZero)
    r = sZero;
  else if (j == sZero)
    r = i;
  else if (i == j)
    r = sZero;
  else {
    if (i->mVariable > j->mVariable)
      r = cache->createNode(i->mVariable,
                            i->mMult,
                            diffShared(cache, i->mAdd, j));
    else if (i->mVariable < j->mVariable)
      r = diffShared(cache, i, j->mAdd);
    else {
      const Node *m=diffShared(cache, i->mMult, j->mMult),
                 *a=diffShared(cache, i->mAdd, j->mAdd);
      if (m == sZero)
        r = a;
      else
        r = cache->createNode(i->mVariable, m, a);
    }
  }
  return r;
}

#ifdef GINV_ZDD_GRAPHVIZ
void ZDD::ConstIterator::saveImage(Agraph_t *g)  {
  char buffer[256];
  while(mNode && mNode->mVariable >= 0) {
    sprintf(buffer, "%p", mNode);
//     Agnode_t *r=agnode(g, buffer);
    Agnode_t *r=agfindnode(g, buffer);
    if (r == NULL) {
      r = agnode(g, buffer);
      sprintf(buffer, "%d", variable());
      agsafeset(r, (char*)"label", buffer, (char*)"");
    }

    Agnode_t *m;
    if (mult().variable() >= 0) {
      sprintf(buffer, "%p", mult().mNode);
      m = agfindnode(g, buffer);
      if (m == NULL) {
        m = agnode(g, buffer);
        sprintf(buffer, "%d", mult().variable());
        agsafeset(m, (char*)"label", buffer, (char*)"");
        mult().saveImage(g);
      }
    }
    else {
      assert(mult().isOne());
      m = agnode(g, (char*)"1");
      agsafeset(m, (char*)"shape", (char*)"box", (char*)"");
    }

    Agnode_t *a;
    bool exit=false;
    if (add().variable() >= 0) {
      sprintf(buffer, "%p", add().mNode);
      a = agfindnode(g, buffer);
      if (a)
        exit = true;
      else {
        a = agnode(g, buffer);
        sprintf(buffer, "%d", add().variable());
        agsafeset(a, (char*)"label", (char*)buffer, (char*)"");
      }
    }
    else {
      if (add().isZero())
        a = agnode(g, (char*)"0");
      else {
        assert(add().isOne());
        a = agnode(g, (char*)"1");
      }
      agsafeset(a, (char*)"shape", (char*)"box", (char*)"");
    }

//     Agedge_t *e=agfindedge(g, r, m);
//     if (e == NULL) {
//       e = agedge(g, r, m);
//       agsafeset(e, (char*)"style", (char*)"solid", (char*)"");
//       agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
//     }
//     e = agfindedge(g, r, a);
//     if (e == NULL) {
//       e 	= agedge(g, r, a);
//       agsafeset(e, (char*)"style", (char*)"dashed", (char*)"");
//       agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
//     }
    Agedge_t *e=agedge(g, r, m);
    agsafeset(e, (char*)"style", (char*)"solid", (char*)"");
    agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
    e = agedge(g, r, a);
    agsafeset(e, (char*)"style", (char*)"dashed", (char*)"");
    agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
    if (exit)
      break;
 
    mNode = mNode->mAdd;
  }
}
#endif // GINV_ZDD_GRAPHVIZ

#ifdef GINV_ZDD_GRAPHVIZ
void ZDD::saveImage(const char* format, const char* filename) const {
  GVC_t *gvc=gvContext();
  Agraph_t *g=agopen((char*)"ZDD", AGDIGRAPH);
  ConstIterator(*this).saveImage(g);
  gvLayout(gvc, g, (char*)"dot");
  gvRenderFilename(gvc, g, format, filename);
  gvFreeLayout(gvc, g);
  agclose(g);
}
#endif // GINV_ZDD_GRAPHVIZ

void ZDD::printPull(RBTree<PrintNode*, ZDD::PrintNode::compare> &cache, const Node *i, Allocator* allocator) {
  assert(i && i->mVariable >= 0);
  do {
    PrintNode n(i);
    RBTree<PrintNode*, ZDD::PrintNode::compare>::ConstIterator f=cache.find(&n);
    if (!f)
      cache.insert(new(allocator) PrintNode(i));
    else {
      ++f.key()->mRef;
      if (f.key()->mRef > 1)
        break;
    }
    if (i->mMult->mVariable >= 0)
      printPull(cache, i->mMult, allocator);
    i = i->mAdd;
    assert(i);
  } while(i->mVariable >= 0);
}

void ZDD::printPush(std::ostream& out, RBTree<PrintNode*, ZDD::PrintNode::compare> &cache, const Node *i, bool bracket) {
  assert(i && i->mVariable >= 0);
  PrintNode n(i);
  RBTree<PrintNode*, ZDD::PrintNode::compare>::ConstIterator f=cache.find(&n);
  bracket = bracket && f.key()->mRef == 1 && (i->mAdd->mVariable >= 0 || i->mAdd == sOne);
  if (bracket)
    out << '(';
  do  {
    PrintNode n(i);
    RBTree<PrintNode*, ZDD::PrintNode::compare>::ConstIterator f=cache.find(&n);
    assert(f);
    if (f.key()->mRef > 1) {
      out << "p[" << f.key()->mNumber << ',' << f.key()->mRef << ']';
      break;
    }
    else {
      out << 'x' << i->mVariable;
      assert(i->mMult->mVariable >= 0 || i->mMult == sOne);
      if (i->mMult->mVariable >= 0)
        printPush(out, cache, i->mMult, true);
    }
    i = i->mAdd;
    assert(i);
    if (i->mVariable >= 0)
      out << '+';
    else {
      if (i == sOne)
        out << "+1";
      break;
    }
  } while(true);
  if (bracket)
    out << ')';
}

std::ostream& operator<<(std::ostream& out, const ZDD &a) {
  if (a.isZero())
    out << '0';
  else if (a.isOne())
    out << '1';
  else {
    Allocator allocator[1];
    RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare> cache(allocator);
    ZDD::printPull(cache, a.mRoot, allocator);
    int k=0;
    for(RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare>::ConstIterator i(cache.minimum()); i; ++i)
      if (i.key()->mRef > 1)
        i.key()->mNumber = ++k;
    ZDD::printPush(out, cache, a.mRoot, false);
    out << std::endl;
    for(RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare>::ConstIterator i(cache.minimum()); i; ++i)
      if (i.key()->mRef > 1) {
        out << "p[" << i.key()->mNumber << ',' << i.key()->mRef << "]=x" << i.key()->mNode->mVariable;
        if (i.key()->mNode->mMult->mVariable >= 0)
          ZDD::printPush(out, cache, i.key()->mNode->mMult, true);
        if (i.key()->mNode->mAdd->mVariable >= 0) {
          out << '+';
          ZDD::printPush(out, cache, i.key()->mNode->mAdd, false);
        }
        else if (i.key()->mNode->mAdd == ZDD::sOne)
          out << "+1";
        out << std::endl;
      }
    for(RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare>::ConstIterator i(cache.minimum()); i; ++i)
       allocator->dealloc(i.key());
  }
  return out;
}

}

