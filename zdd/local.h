/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_ZDDLOCAL_H
#define GINV_ZDDLOCAL_H

#include "zdd.h"

namespace GInv {

class ZDDLocal: public ZDD {
public:
  ZDDLocal(Allocator *allocator):
      ZDD(new(allocator) ZDD::Cache(allocator)) {
  }
  ZDDLocal(const ZDDLocal &a, Allocator *allocator):
      ZDD(a, new(allocator) ZDD::Cache(allocator)) {
  }
  ZDDLocal(const ZDD &a, Allocator *allocator):
      ZDD(a, new(allocator) ZDD::Cache(allocator)) {
  }
  ~ZDDLocal() {
    mCache->allocator()->destroy(mCache);
  }

  inline void swap(ZDDLocal &a) { ZDD::swap(a);}
  void operator=(const ZDDLocal &a) {
    assert(this != &a);
    mRoot = copy(mCache, a.mRoot);
  }

  inline void add(const ZDDLocal& a) {
    assert(this != &a);
    mRoot = addLocal(mCache, mRoot, a.mRoot);
  }
  inline void add(const ZDDLocal& a, const ZDDLocal& b) {
    assert(this != &a && this != &b && &a != &b);
    mRoot = addLocal(mCache, a.mRoot, b.mRoot);
  }
  inline void mult(Variable v) {
    assert(v >= 0);
    mRoot = multShared(mCache, mRoot, v);
  }
  inline void mult(const ZDDLocal& a) {
    assert(this != &a);
    mRoot = multLocal(mCache, mRoot, a.mRoot);
  }
  inline void mult(const ZDDLocal& a, const ZDDLocal& b) {
    assert(this != &a && this != &b && &a != &b);
    mRoot = multLocal(mCache, a.mRoot, b.mRoot);
  }
  inline void mult(const ZDDLocal& a, Variable v) {
    assert(this != &a);
    assert(v >= 0);
    mRoot = multLocal(mCache, a.mRoot, v);
  }
  inline void union_(const ZDDLocal& a) {
    assert(this != &a);
    mRoot = unionLocal(mCache, mRoot, a.mRoot);
  }
  inline void union_(const ZDDLocal& a, const ZDDLocal& b) {
    assert(this != &a && this != &b && &a != &b);
    mRoot = unionLocal(mCache, a.mRoot, b.mRoot);
  }
  inline void diff(const ZDDLocal& a) {
    assert(this != &a);
    mRoot = diffLocal(mCache, mRoot, a.mRoot);
  }
  inline void diff(const ZDDLocal& a, const ZDDLocal& b) {
    assert(this != &a && this != &b && &a != &b);
    mRoot = diffLocal(mCache, a.mRoot, b.mRoot);
  }
  inline void intersection(const ZDDLocal& a) {
    assert(this != &a);
    mRoot = intersectionLocal(mCache, mRoot, a.mRoot);
  }
  inline void intersection(const ZDDLocal& a, const ZDDLocal& b) {
    assert(this != &a && this != &b && &a != &b);
    mRoot = intersectionLocal(mCache, a.mRoot, b.mRoot);
  }
};

typedef GC<ZDDLocal> GCZDDLocal;

}

#endif // GINV_ZDDLOCAL_H
