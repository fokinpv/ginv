/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "zdd/local.h"

using namespace GInv;

class TestZDDLocal: public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(TestZDDLocal);
//   CPPUNIT_TEST(test1);
//   CPPUNIT_TEST(test2);
//   CPPUNIT_TEST(test3);
//   CPPUNIT_TEST(test4);
//   CPPUNIT_TEST(test5);
//   CPPUNIT_TEST(test6);
//   CPPUNIT_TEST(test7);
  CPPUNIT_TEST(test8);  
  CPPUNIT_TEST_SUITE_END();

  int n;

public:
  void setUp();
  void tearDown();

  void test1();
  void test2();
  void test3();
  void test4();
  void test5();
  void test6();
  void test7();
  void test8();  
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestZDDLocal);

void TestZDDLocal::setUp() {
  n = 8;
}

void TestZDDLocal::tearDown() {
}

void TestZDDLocal::test1() {
  GCZDDLocal a;
  CPPUNIT_ASSERT(a.isZero());
  a.setOne();
  CPPUNIT_ASSERT(a.isOne());
  a.setZero();
  CPPUNIT_ASSERT(!a);
  GCZDDLocal b;
  a.setOne();
  b.setOne();
  a.add(b);
  CPPUNIT_ASSERT(a.isZero());
}

void TestZDDLocal::test2() {
  GCZDDLocal sum, tmp;
  for(int i=0; i < n; i++) {
    GCZDDLocal v;
    v.setVariable(i);
    sum.add(v);
    CPPUNIT_ASSERT(sum.cache()->count() == i+1);
  }
  CPPUNIT_ASSERT(sum.cache()->count() == n);
  for(int i=n-1; i > 0; i--) {
    sum.mult(i);
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == n);
  }
  sum.mult(0);
  if (n & 1)
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == n);
  else {
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == 0);
    CPPUNIT_ASSERT(sum.isZero());
  }
}

void TestZDDLocal::test3() {
  GCZDDLocal sum;
  for(int i=0; i < n; i++) {
    GCZDDLocal v;
    v.setVariable(i);
    sum.add(v);
    CPPUNIT_ASSERT(sum.cache()->count() == i+1);
  }
  CPPUNIT_ASSERT(sum.cache()->count() == n);
  for(int i=0; i < n-1; i++) {
    sum.mult(i);
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == n);
  }
  sum.mult(n-1);
  if (n & 1)
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == n);
  else {
    CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == 0);
    CPPUNIT_ASSERT(sum.isZero());
  }
}

static void homogeneous(GCZDDLocal &a, int n1, int n2, int d) {
  assert(0 <= d && d <= n2);
  assert(a.isZero());
  if (d == 0)
    a.setOne();
  else if (d == 1) {
    for(; n1 < n2; n1++) {
      GCZDDLocal v;
      v.setVariable(n1);
      a.add(v);
    }
  }
  else {
    for(; n1 < n2; n1++) {
      GCZDDLocal b;
      homogeneous(b, n1+1, n2, d-1);
      b.mult(n1);
      a.add(b);
    }
  }
}

void TestZDDLocal::test4() {
  for(int i=0; i <= n; i++) {
    GCZDDLocal a;
    homogeneous(a, 0, n, i);
    CPPUNIT_ASSERT(GCZDDLocal(a).cache()->count() == i*(n+1-i));
//     std::cout << a << std::endl;
#ifdef GINV_ZDD_GRAPHVIZ
    char buffer[128];
    sprintf(buffer, "testLocal4_%d.pdf", i);
    a.saveImage("pdf", (const char*)buffer);
    sprintf(buffer, "testLocal4_%d_cache.pdf", i);
    a.cache()->saveImage("pdf", (const char*)buffer);
#endif // GINV_ZDD_GRAPHVIZ
  }
}

void TestZDDLocal::test5() {
  GCZDDLocal sum;
  for(int i=n; i >= 0; i--) {
    GCZDDLocal a, b;
    homogeneous(a, 0, n, i);
    CPPUNIT_ASSERT(GCZDDLocal(a).cache()->count() == i*(n+1-i));
    b = sum;
    sum.add(a, b);
#ifdef GINV_ZDD_GRAPHVIZ
    char buffer[128];
    sprintf(buffer, "test5Local_%d.pdf", i);
    sum.saveImage("pdf", (const char*)buffer);
    sprintf(buffer, "test5Local_%d_cache.pdf", i);
    sum.cache()->saveImage("pdf", (const char*)buffer);
#endif // GINV_ZDD_GRAPHVIZ
  }
  CPPUNIT_ASSERT(GCZDDLocal(sum).cache()->count() == n);
}

void TestZDDLocal::test6() {
  for(int i=0; i <= n; i++) {
    GCZDDLocal a;
    homogeneous(a, 0, n, i);
    CPPUNIT_ASSERT(GCZDDLocal(a).cache()->count() == i*(n+1-i));
    std::cout << i << ", " << i*(n+1-i) << "->" << a << std::endl;
  }
  GCZDDLocal p, r0, r1, v;
  r0.setOne();
  v.setVariable(0);
  r0.add(v);
  r1.mult(r0, 1);
  p.mult(r1, 2);
  p.add(r1);
  p.add(r0);
  std::cout << p << std::endl;
}

void TestZDDLocal::test7() {
  GCZDDLocal a;
  a.setVariable(0);

  GCZDDLocal b;
  b.setVariable(1);

  //GCZDDLocal r;

  a.mult(b);
  std::cout << a << std::endl;
}


void TestZDDLocal::test8() {
  for(int i=1; i < n; i++) {
    GCZDDLocal a;
    homogeneous(a, 0, n, i);
    CPPUNIT_ASSERT(GCZDDLocal(a).cache()->count() == i*(n+1-i));
//     std::cout << i << ", " << i*(n+1-i) << "->" << a << std::endl;

    GCZDDLocal p, r0, r1, v;
//     r0.setOne();
    v.setVariable(i);
    a.add(v);
    r1.mult(a, i+1);
    p.mult(r1, i-1);
    p.add(r1);
    p.add(a);
    std::cout << "test8 " << p << std::endl;

#ifdef GINV_ZDD_GRAPHVIZ
    char buffer[128];
    sprintf(buffer, "test8Local_%d.pdf", i);
    p.saveImage("pdf", (const char*)buffer);
    sprintf(buffer, "test8Local_%d_cache.pdf", i);
    p.cache()->saveImage("pdf", (const char*)buffer);
#endif // GINV_ZDD_GRAPHVIZ
    
  }
}
