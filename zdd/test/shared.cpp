/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "zdd/shared.h"
#include "zdd/local.h"

using namespace GInv;

class TestZDDShared: public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(TestZDDShared);
  CPPUNIT_TEST(test1);
  CPPUNIT_TEST(test2);
  CPPUNIT_TEST(test3);
  CPPUNIT_TEST(test4);
  CPPUNIT_TEST(test5);
  CPPUNIT_TEST_SUITE_END();

  int n;

public:
  void setUp();
  void tearDown();

  void test1();
  void test2();
  void test3();
  void test4();
  void test5();
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestZDDShared);

void TestZDDShared::setUp() {
  n = 8;
}

void TestZDDShared::tearDown() {
}

void TestZDDShared::test1() {
  Allocator allocator[1];
  ZDD::Cache cache(allocator);

  ZDDShared a(&cache);
  CPPUNIT_ASSERT(a.isZero());
  a.setOne();
  CPPUNIT_ASSERT(a.isOne());
  a.setZero();
  CPPUNIT_ASSERT(!a);
  ZDDShared b(&cache);
  a.setOne();
  b.setOne();
  a.add(b);
  CPPUNIT_ASSERT(a.isZero());
}

void TestZDDShared::test2() {
  Allocator allocator[1];
  ZDD::Cache cache(allocator);

  ZDDShared sum(&cache), tmp(&cache);
  for(int i=0; i < n; i++) {
    ZDDShared v(&cache);
    v.setVariable(i);
    sum.add(v);
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == i+1);
  }
  CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  for(int i=n-1; i > 0; i--) {
    sum.mult(i);
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  }
  sum.mult(0);
  if (n & 1)
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  else {
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == 0);
    CPPUNIT_ASSERT(sum.isZero());
  }
}

void TestZDDShared::test3() {
  Allocator allocator[1];
  ZDD::Cache cache(allocator);

  ZDDShared sum(&cache);
  for(int i=0; i < n; i++) {
    ZDDShared v(&cache);
    v.setVariable(i);
    sum.add(v);
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == i+1);
  }
  CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  for(int i=0; i < n-1; i++) {
    sum.mult(i);
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  }
  sum.mult(n-1);
  if (n & 1)
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
  else {
    CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == 0);
    CPPUNIT_ASSERT(sum.isZero());
  }
}

static void homogeneous(ZDDShared &a, ZDD::Cache *cache, int n1, int n2, int d) {
  assert(0 <= d && d <= n2);
  assert(a.isZero());
  if (d == 0)
    a.setOne();
  else if (d == 1) {
    for(; n1 < n2; n1++) {
      ZDDShared v(cache);
      v.setVariable(n1);
      a.add(v);
    }
  }
  else {
    for(; n1 < n2; n1++) {
      ZDDShared b(cache);
      homogeneous(b, cache, n1+1, n2, d-1);
      b.mult(n1);
      a.add(b);
    }
  }
}

void TestZDDShared::test4() {
  Allocator allocator[1];
  ZDD::Cache cache(allocator);

  for(int i=0; i <= n; i++) {
    ZDDShared a(&cache);
    homogeneous(a, &cache, 0, n, i);
    CPPUNIT_ASSERT(ZDDLocal(a, allocator).cache()->count() == i*(n+1-i));
#ifdef GINV_ZDD_GRAPHVIZ
    char buffer[128];
    sprintf(buffer, "test4Shared_%d.pdf", i);
    a.saveImage("pdf", (const char*)buffer);
    sprintf(buffer, "test4Shared_%d_cache.pdf", i);
    a.cache()->saveImage("pdf", (const char*)buffer);
#endif // GINV_ZDD_GRAPHVIZ
  }
}

void TestZDDShared::test5() {
  Allocator allocator[1];
  ZDD::Cache cache(allocator);

  ZDDShared sum(&cache);
  for(int i=n; i >= 0; i--) {
    ZDDShared a(&cache), b(&cache);
    homogeneous(a, &cache, 0, n, i);
    CPPUNIT_ASSERT(ZDDLocal(a, allocator).cache()->count() == i*(n+1-i));
    b = sum;
    sum.add(a, b);
#ifdef GINV_ZDD_GRAPHVIZ
    char buffer[128];
    sprintf(buffer, "test5Shared_%d.pdf", i);
    sum.saveImage("pdf", (const char*)buffer);
    sprintf(buffer, "test5Shared_%d_cache.pdf", i);
    sum.cache()->saveImage("pdf", (const char*)buffer);
#endif // GINV_ZDD_GRAPHVIZ
  }
  CPPUNIT_ASSERT(ZDDLocal(sum, allocator).cache()->count() == n);
}
