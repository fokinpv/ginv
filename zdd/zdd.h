/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_ZDD_ZDD_H
#define GINV_ZDD_ZDD_H

#include <cassert>
#include <iostream>
#include <sstream>

#include "config.h"

#ifdef GINV_ZDD_GRAPHVIZ
  #include <graphviz/gvc.h>
  #include <cstdio>
#endif // GINV_ZDD_GRAPHVIZ

#include "util/list.h"
#include "util/rbtree.h"

namespace GInv {

class ZDD {
public:
  typedef int Variable;

protected:
  struct Node {
    Variable     mVariable;
    const Node*  mMult;
    const Node*  mAdd;

    Node():
        mVariable(-1),
        mMult(NULL),
        mAdd(NULL) {
    }
    Node(Variable v, const Node* m, const Node* a):
        mVariable(v),
        mMult(m),
        mAdd(a) {
      assert(mVariable >= 0);
      assert(mVariable > mMult->mVariable && mVariable > mAdd->mVariable);
    }
    ~Node() {}

    inline int compare(const Node* a) const {
      int r=mVariable - a->mVariable;
      if (r == 0) {
        r = mMult->mVariable - a->mMult->mVariable;
        if (r == 0)
          r = mAdd->mVariable - a->mAdd->mVariable;
      }
      return r;
    }
  };
  static inline int compare(List<const ZDD::Node*>* a, List<const ZDD::Node*>* b) {
    return a->begin().data()->compare(b->begin().data());
  }

public:
  class Cache {
  protected:
    typedef List<const ZDD::Node*> Chain;

    Allocator*               mAllocator;
    RBTree<Chain*, compare>  mCache;
    size_t                   mCollision;
    size_t                   mCount;

    Chain                    mTmp1;
    Node                     mTmp2;

  public:
    Cache(Allocator *allocator):
        mAllocator(allocator),
        mCache(mAllocator),
        mCollision(1),
        mCount(0),
        mTmp1(mAllocator) {
      mTmp1.begin().insert(&mTmp2);
    }
    ~Cache();

    void swap(Cache& a);

    const Node* createNode(Variable v, const Node* m, const Node* a);

    size_t collision() const { return mCollision-1; }
    size_t cacheSize() const { return mCache.length(); }
    size_t count() const { return mCount; }

    const Allocator* allocator() const { return mAllocator; }
    Allocator* allocator() { return mAllocator; }

#ifdef GINV_ZDD_GRAPHVIZ
    void saveImage(const char* format, const char* filename) const;
#endif // GINV_ZDD_GRAPHVIZ
  };

protected:
  static const Node *const sZero;
  static const Node *const sOne;

  Cache*       mCache;
  const Node*  mRoot;

  static const Node* copy(Cache *cache, const Node *i);
  // monomial operations
  template <typename VariableIterator>
  static const Node* addMonomLocal(Cache *cache, const Node *i, VariableIterator &vars);
  template <typename VariableIterator>
  static const Node* addMonomShared(Cache *cache, const Node *i, VariableIterator &vars);
  template <typename VariableIterator>
  static const Node* multMonomLocal(Cache *cache, const Node *i, VariableIterator &vars);
  template <typename VariableIterator>
  static const Node* multMonomShared(Cache *cache, const Node *i, VariableIterator &vars);
  template <typename VariableIterator>
  static bool isMember(const Node *i, VariableIterator &vars);
  // polynomial operations
  static const Node* addLocal(Cache *cache, const Node *i, const Node *j);
  static const Node* addShared(Cache *cache, const Node *i, const Node *j);
  static const Node* multLocal(Cache *cache, const Node *i, Variable v);
  static const Node* multLocal(Cache *cache, const Node *i, const Node* j);
  static const Node* multShared(Cache *cache, const Node *i, Variable v);
  static const Node* multShared(Cache *cache, const Node *i, const Node* j);
  // set operations
  static const Node* unionLocal(Cache *cache, const Node *i, const Node *j);
  static const Node* unionShared(Cache *cache, const Node *i, const Node *j);
  static const Node* intersectionLocal(Cache *cache, const Node *i, const Node *j);
  static const Node* intersectionShared(Cache *cache, const Node *i, const Node *j);
  static const Node* diffLocal(Cache *cache, const Node *i, const Node *j);
  static const Node* diffShared(Cache *cache, const Node *i, const Node *j);

  struct PrintNode {
    const Node* mNode;
    int         mRef;
    int         mNumber;

    PrintNode(const Node* node):
        mNode(node),
        mRef(1),
        mNumber(0) {
    }
    ~PrintNode() {}

    static inline int compare(PrintNode *a, PrintNode *b) {
      int r=0;
      if (a->mNode > b->mNode)
        r = 1;
      else if (a->mNode < b->mNode)
        r = -1;
      else
        r = 0;
      return r;
    }
  };

  static void printPull(RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare> &cache, const Node *i, Allocator* allocator);
  static void printPush(std::ostream& out, RBTree<ZDD::PrintNode*, ZDD::PrintNode::compare> &cache, const Node *i, bool bracket);

public:
  class ConstIterator {
    const ZDD::Node* mNode;

    ConstIterator(const ZDD::Node* node):
        mNode(node) {
    }

  public:
    ConstIterator():
        mNode(NULL) {
    }
    ConstIterator(const ZDD& a):
        mNode(a.mRoot) {
    }
    ~ConstIterator() {}

    operator bool() const { return mNode && mNode->mVariable >= 0; }
    bool isZero() const { return mNode == sZero; }
    bool isOne() const { return mNode == sOne; }
    Variable variable() const {
      assert(mNode != NULL);
      return mNode->mVariable;
    }
    ConstIterator mult() const {
      assert(mNode != NULL);
      return mNode->mMult;
    }
    ConstIterator add() const {
      assert(mNode != NULL);
      return mNode->mAdd;
    }

    friend bool operator ==(const ConstIterator& a, const ConstIterator& b) {
      return a.mNode == b.mNode;
    }
    friend bool operator !=(const ConstIterator& a, const ConstIterator& b) {
      return a.mNode != b.mNode;
    }

#ifdef GINV_ZDD_GRAPHVIZ
    void saveImage(Agraph_t *g);
#endif // GINV_ZDD_GRAPHVIZ
  };

  ZDD():
      mCache(NULL),
      mRoot((Node*)sZero) {
  }
  ZDD(Cache *cache):
      mCache(cache),
      mRoot((Node*)sZero) {
    assert(cache);
  }
  ZDD(const ZDD& a, Cache *cache):
      mCache(cache),
      mRoot(NULL) {
    assert(cache);
    if (mCache == a.mCache)
      mRoot = a.mRoot;
    else
      mRoot = copy(mCache, a.mRoot);
  }
  ~ZDD() {}

  const Cache* cache() const { return mCache; }
  Cache* cache() { return mCache; }

  void swap(ZDD &a) {
    Cache *tmp1=mCache;
    mCache = a.mCache;
    a.mCache = tmp1;

    const Node* tmp2=mRoot;
    mRoot = a.mRoot;
    a.mRoot = tmp2;
  }
  void operator=(const ZDD &a) {
    assert(a.mCache != NULL);
    if (mCache == NULL)
      mCache = a.mCache;

    if (mCache == a.mCache)
      mRoot = a.mRoot;
    else
      mRoot = copy(mCache, a.mRoot);
  }

  operator bool() const { return mRoot != sZero; }
  bool isZero() const { return mRoot == sZero; }
  bool isOne() const { return mRoot == sOne; }

  void setZero() { mRoot = (Node*)sZero; }
  void setOne() { mRoot = (Node*)sOne; }
  void setVariable(Variable v) {
    assert(v >= 0);
    mRoot = mCache->createNode(v, sOne, sZero);
  }
  //TODO Cython __repr__
  std::string repr()
  {
    std::stringstream s;
    s << *this;
    return s.str();
  }

#ifdef GINV_ZDD_GRAPHVIZ
  void saveImage(const char* format, const char* filename) const;
#endif // GINV_ZDD_GRAPHVIZ

  friend std::ostream& operator<<(std::ostream& out, const ZDD &a);
};

}

#endif // GINV_ZDD_ZDD_H
