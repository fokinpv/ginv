/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_ZDDSHARED_H
#define GINV_ZDDSHARED_H

#include "zdd.h"

namespace GInv {

class ZDDShared: public ZDD {
public:
  ZDDShared(Cache *cache):
      ZDD(cache) {
  }
  ZDDShared(const ZDDShared &a, Cache *cache):
      ZDD(a, cache) {
  }
  ZDDShared(const ZDD &a, Cache *cache):
      ZDD(a, cache) {
  }
  ~ZDDShared() {}

  inline void swap(ZDDShared &a) { ZDD::swap(a);}
  void operator=(const ZDDShared &a) {
    assert(mCache == a.mCache);
    mRoot = a.mRoot;
  }

  inline void add(const ZDDShared& a) {
    assert(this != &a);
    assert(mCache == a.mCache);
    mRoot = addShared(mCache, mRoot, a.mRoot);
  }
  inline void add(const ZDDShared& a, const ZDDShared& b) {
    assert(this != &a && this != &b && &a != &b);
    assert(mCache == a.mCache && mCache == b.mCache);
    mRoot = addShared(mCache, a.mRoot, b.mRoot);
  }
  inline void mult(Variable v) {
    assert(v >= 0);
    mRoot = multShared(mCache, mRoot, v);
  }
  inline void mult(const ZDDShared& a, Variable v) {
    assert(this != &a);
    assert(mCache == a.mCache);
    assert(v >= 0);
    mRoot = multShared(mCache, a.mRoot, v);
  }
  inline void mult(const ZDDShared& a, const ZDDShared& b) {
    assert(this != &a && this != &b && &a != &b);
    assert(mCache == a.mCache && mCache == b.mCache);
    mRoot = multShared(mCache, a.mRoot, b.mRoot);
  }
};

}

#endif // GINV_ZDDSHARED_H
