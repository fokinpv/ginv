# -*- coding: utf-8 -*-

import time

from poly import *

def janet(w1, w2, B):
  if w1[0].lm().lex(w2[0].lm()) < 0:
    w1, w2 = w2, w1
  for i in range(len(Monom.variables)):
    if w1[0].lm()[i] != w2[0].lm()[i]:
      if not w2[2][i]:
        w2[2][i] = False
        B.append([w2[0].lm().prolong(i), w2[0], i, w[1]])
      break

class Invo(list):
  def __init__(self):
    list.__init__(self)

  def __str__(self):
    return ",\n".join(str(w[0]) for w in self)

  def __insert(self, w):
    assert not self.find(w[0].lm())
    i = 0
    while i < len(self) and cmp(w[0].lm(), self[i][0].lm()) > 0:
      janet(w, self[i], self.B)
      i += 1
    self.insert(i, w)
    i += 1
    while i < len(self):
      if self[i].lm().divisible(w[0].lm()):
        self.F.append(self.pop(i))
      else:
        self[i][0].NFtail(self)
        self[i][0].pp()
        janet(w, self[i], self.B)
        i += 1
    assert self.assertValid()

  def find(self, m):
    for w in self:
      if m.divMulti(w[0].lm(), w[2]):
        return w[0]
    return None

  def __criterion(self, m, ans):
    #for w in self:
      #if m.divMulti(w[0].lm(), w[2]):
        #return w
    return None

  def algorithm1(self, F):
    assert Monom.__cmp__ == Monom.lex
    self.time, self.crit1, self.crit2 = time.time(), 0, 0,
    self.F, self.B = [[f, f.lm(), [True for c in Monom.variables]] for f in F], []
    while self.F or self.B:
      w, i = None, 0
      while i < len(self.F):
        print i, self.F[i][0].lm(),
        self.F[i][0].NFhead(self)
        print '->', self.F[i][0].lm() if self.F[i][0] else '0'
        if not self.F[i][0]:
          del self.F[i]
        else:
          self.F[i][0].pp()
          if not w or cmp(self.F[i][0].lm(), w[0].lm()) < 0:
            w = self.F[i]
            print w
          i += 1
      if w:
        F.remove(w)
      else:
        self.B.sort()
        while self.B and not self.F:
          m, p, d, ans = self.B[0]
          if p and m == p.lm().prolong(d) and not self.__criterion(m, ans):
            p.NFhead(self)
            if p:
              p.pp()
              w = [p, ans if m == p.lm() else p.lm(), [True for c in Monom.variables]]
              F.append(w)
              print F
          del self.B[0]
      if w:
        print w
        if w[0].lm().degree() == 0:
          self = [Poly(1), Monom.zero, [True for c in Monom.variables]]
          break
        w[0].NFtail(self)
        w[0].pp()
        print "len(F) =", len(F), " w[0].lm() =", w[0].lm()
        self.__insert(w)
    del self.B
    del self.F
    self.time = time.time() - self.time

  def algorithm2(self, F):
    assert Monom.__cmp__ == Monom.deglex
    self.time, self.crit1, self.crit2 = time.time(), 0, 0,
    self.F, self.B = F, []
    while F or self.B:
      self.B.sort()
      while self.B:
        m, p1, p2 = self.B[0]
        if p1 and p2 and m == p1.lm().lcm(p2.lm()) and not self.__crit2(m, p1, p2):
          p = Poly.S(p1, p2)
          if p:
            p.pp()
            F.append(p)
        del self.B[0]
      p, i = None, 0
      while i < len(F):
        print i, F[i].lm(),
        F[i].NFhead(self)
        print '->', F[i].lm() if F[i] else '0'
        if not F[i]:
          del F[i]
        else:
          F[i].pp()
          if not p or cmp(F[i].lm(), p.lm()) < 0:
            p = F[i]
          i += 1
      if p:
        if p.lm().degree() == 0:
          self = [Poly(1)]
          break
        F.remove(p)
        p.NFtail(self)
        p.pp()
        print "len(F) =", len(F), " p.lm() =", p.lm()
        self.__insert(p)
    del self.B
    del self.F
    self.time = time.time() - self.time

  def assertValid(self):
    for i in range(len(self)-1):
      if not self[i] or cmp(self[i].lm(), self[i+1].lm()) >= 0:
        return False
    return not self or self[-1]

if __name__ == '__main__':
  Monom.variables = ['a', 'b', 'c']
  Monom.zero = Monom(0 for v in Monom.variables)
  Monom.__cmp__ = Monom.lex
  for i in range(len(Monom.variables)):
    p = Poly()
    p.append([Monom(0 if l != i else 1 for l in range(len(Monom.variables))), 1])
    globals()[Monom.variables[i]] = p

  I = Invo()
  I.algorithm1([a**3 - b**2 + c - 1, b**3 - c**2 + a - 1, c**3 - a**2 + b - 1])
  print I
  print ", ".join(str(g[0].lm()) for g in I if g[0].lm() == g[1])
  print "crit1 =", I.crit1, "crit2 =", I.crit2
  print "time %.2f" % I.time
