#!/usr/bin/env python
#-*- coding: utf-8 -*-

import pyzdd

class ZDDLocal(pyzdd.PyGCZDDLocal):  
  pass  

class Allocator(pyzdd.PyAllocator):
  pass

class Cache(pyzdd.PyCache):
  pass

class ZDDShared(pyzdd.PyZDDShared):
  pass

if __name__ == "__main__":

  x1 = ZDDLocal(1)
  x2 = ZDDLocal(2)
  x3 = ZDDLocal(3)

  x1.saveImage("pdf", "test1.pdf")

  r = x1*x2
  print r
  r.saveImage("pdf", "test1.pdf")
  
  for i in xrange(80):
    globals()['x%d' % i] = ZDDLocal(i)

  r = x1*(x1 + x2 + x3) + x3*(x4 + x5 + x6) 

  print r
  r.reallocate()
  print r.size()
  r.saveImage("pdf", "test2.pdf")
  
  #TEST ZDDShared
  print "TEST ZDDShared"

  allocator = Allocator()
  cache = Cache(allocator)

  zddshared1 = ZDDShared(cache, 1)
  zddshared2 = ZDDShared(cache, 2)
  r1 = ZDDShared(cache)
  r1 = zddshared1 + zddshared2
  print r1
  r1.saveImage("pdf", "test3.pdf")


