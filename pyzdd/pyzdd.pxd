from libcpp.string cimport string
ctypedef int Variable

cdef extern from "../zdd/local.h" namespace "GInv":
  ctypedef char* const_char_ptr "const char*"
  cdef cppclass ZDDLocal:
    ZDDLocal()

cdef extern from "../util/allocator.h" namespace "GInv":
  cdef cppclass GC[ZDDLocal]:
    GC()
    void setVariable(Variable)
    void setZero()
    void setOne()
    string repr()
    void add(GC[ZDDLocal])
    void add(GC[ZDDLocal], GC[ZDDLocal])
    void mult(Variable)
    void mult(GC[ZDDLocal], Variable)
    void mult(GC[ZDDLocal])
    void mult(GC[ZDDLocal], GC[ZDDLocal])
    void union_(GC[ZDDLocal])
    void union_(GC[ZDDLocal], GC[ZDDLocal])
    void diff(GC[ZDDLocal])
    void diff(GC[ZDDLocal], GC[ZDDLocal])
    void intersection(GC[ZDDLocal])
    void intersection(GC[ZDDLocal], GC[ZDDLocal])
    void saveImage(const_char_ptr, const_char_ptr)
    void reallocate()
    size_t size()
  cdef cppclass Allocator:
    Allocator()
    size_t size()

cdef extern from "../zdd/zdd.h" namespace "GInv::ZDD":
  cdef cppclass Cache:
    Cache(Allocator* allocator)

cdef extern from "../zdd/shared.h" namespace "GInv":
  ctypedef char* const_char_ptr "const char*"
  cdef cppclass ZDDShared:
    ZDDShared(Cache* cache)
    void setVariable(Variable)
    void setZero()
    void setOne()
    void saveImage(const_char_ptr, const_char_ptr)
    string repr()
    void add(ZDDShared)
    void add(ZDDShared, ZDDShared)
    void mult(Variable)
    void mult(ZDDShared, Variable)
    void mult(ZDDShared, ZDDShared)

ctypedef GC[ZDDLocal] GCZDDLocal
