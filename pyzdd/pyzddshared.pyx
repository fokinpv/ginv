from libcpp.string cimport string
from cython.operator cimport dereference as deref

# For dellocation
# http://stackoverflow.com/questions/4501938/how-can-c-object-lifetimes-be-correctly-managed-in-cython

cdef class PyAllocator:

  cdef Allocator *thisptr
  cdef set caches

  def __cinit__(self):
    self.thisptr = new Allocator()
    self.caches = set()

  def __dealloc__(self):
    caches_to_dealloc = self.caches.copy()
    for cache in caches_to_dealloc:
      del cache
    del self.thisptr

  def attach(self, PyCache zdd):
    self.caches.add(zdd)

  def detach(self, PyCache zdd):
    self.caches.remove(zdd)

cdef class PyCache:

  cdef Cache *thisptr
  cdef PyAllocator allocator
  cdef set zddshared

  def __cinit__(self, PyAllocator allocator):
    self.thisptr = new Cache(allocator.thisptr)
    self.zddshared = set()
    self.allocator = allocator
    allocator.attach(self)

  def __dealloc__(self):
    zddshared_to_dealloc = self.zddshared.copy()
    for zdd in zddshared_to_dealloc:
      del zdd
    self.allocator.detach(self)
    del self.thisptr

  def attach(self, PyZDDShared zdd):
    self.zddshared.add(zdd)

  def detach(self, PyZDDShared zdd):
    self.zddshared.remove(zdd)


cdef class PyZDDShared:

  cdef ZDDShared *thisptr
  cdef PyCache cache

  def __cinit__(self, PyCache cache,var=-1):
    self.cache = cache
    self.thisptr = new ZDDShared(cache.thisptr)
    if var != -1:
      self.setVariable(var)
    cache.attach(self)

  def __dealloc__(self):
    self.cache.detach(self)
    del self.thisptr

  def __str__(self):
    cdef string s 
    s = self.thisptr.repr()
    return s.c_str()

  def setOne(self):
    self.thisptr.setOne()
  
  def setZero(self):
    self.thisptr.setZero()

  def setVariable(self,int v):
    self.thisptr.setVariable(v)

  def saveImage(self, format_, filename):
    self.thisptr.saveImage(format_, filename)

  def add(self, PyZDDShared other):
    self.thisptr.add(deref(other.thisptr))

  def __add__(PyZDDShared self, PyZDDShared other):
    self.add(other)
    return self

  def mult(self, Variable v):
    self.thisptr.mult(v)

  def __mul__(PyZDDShared self, PyZDDShared other):
    self.mult(other)
    return self

