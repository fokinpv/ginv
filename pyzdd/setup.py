from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(ext_modules=[Extension(
                   "pyzdd",                 # name of extension
                   ["pyzdd.pxd", "pyzdd.pyx", "../zdd/zdd.cpp", "../util/allocator.cpp", "../util/timer.cpp"],
                   include_dirs=['..', '/usr/include/graphviz/'],
                   define_macros = [("GINV_GRAPHVIZ", 1), ("GINV_ALLOCATOR", 1)],
                   libraries=['gvc', 'graph'],
                   language="c++")],  # causes Cython to create C++ source
      cmdclass={'build_ext': build_ext})
