#-*- coding: utf-8 -*-

import pyzdd

letterFreq = {
  'О':1,  'Т':6,   'К':11,  'Я':16,  'Б':21,  'Ш':26,  'Ф':31,
  'Е':2,  'С':7,   'М':12,  'Ы':17,  'Ч':22,  'Ю':27,  'Ъ':32,
  'А':3,  'Р':8,   'Д':13,  'Ь':18,  'Й':23,  'Ц':28,  'Ё':33,
  'И':4,  'В':9,   'П':14,  'Г':19,  'Х':24,  'Щ':29,
  'Н':5,  'Л':10,  'У':15,  'З':20,  'Ж':25,  'Э':30,
}

n = 24
zdd = pyzdd.PyGCZDDLocal()
#k = 1
for w in open('dal.txt', 'r').readlines():
  word = pyzdd.PyGCZDDLocal()
  word.setOne()
  for i in range(0, len(w)-1, 2):
    word.multV((34 - letterFreq[w[i:i+2]])*n)
  zdd += word
  zdd.reallocate()
  #k += 1
  #if k > 10000:
    #break
zdd.reallocate()
print zdd.size()
#class ZDDLocal(pyzdd.PyGCZDDLocal):
  #pass

#class Allocator(pyzdd.PyAllocator):
  #pass

#class Cache(pyzdd.PyCache):
  #pass

#class ZDDShared(pyzdd.PyZDDShared):
  #pass

#if __name__ == "__main__":

  #x1 = ZDDLocal(1)
  #x2 = ZDDLocal(2)
  #x3 = ZDDLocal(3)

  #x1.saveImage("pdf", "test1.pdf")

  #r = x1*x2
  #print r
  #r.saveImage("pdf", "test1.pdf")

  #for i in xrange(80):
    #globals()['x%d' % i] = ZDDLocal(i)

  #r = x1*(x1 + x2 + x3) + x3*(x4 + x5 + x6)

  #print r
  #r.reallocate()
  #print r.size()
  #r.saveImage("pdf", "test2.pdf")

  ##TEST ZDDShared
  #print "TEST ZDDShared"

  #allocator = Allocator()
  #cache = Cache(allocator)

  #zddshared1 = ZDDShared(cache, 1)
  #zddshared2 = ZDDShared(cache, 2)
  #r1 = ZDDShared(cache)
  #r1 = zddshared1 + zddshared2
  #print r1
  #r1.saveImage("pdf", "test3.pdf")


