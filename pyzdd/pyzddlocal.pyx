from libcpp.string cimport string
from cython.operator cimport dereference as deref
#from pyzdd cimport GCZDDLocal

cdef class PyGCZDDLocal:

  cdef GCZDDLocal *thisptr

  def __cinit__(self, var=-1):
    self.thisptr = new GCZDDLocal()
    if var != -1:
      self.setVariable(var)

  def __dealloc__(self):
    del self.thisptr

  def __str__(self):
    cdef string s
    s = self.thisptr.repr()
    return s.c_str()

  def add(self, PyGCZDDLocal other):
    self.thisptr.add(deref(other.thisptr))

  def __add__(PyGCZDDLocal self, PyGCZDDLocal other):
    r = PyGCZDDLocal()
    r.thisptr.add(deref(self.thisptr), deref(other.thisptr))
    return r

  def multV(self, Variable v):
    self.thisptr.mult(v)

  def mult(self, PyGCZDDLocal other):
    self.thisptr.mult(deref(other.thisptr))

  def __mul__(PyGCZDDLocal self, PyGCZDDLocal other):
    self.mult(other)
    return self

  def union_(self, PyGCZDDLocal other):
    self.thisptr.union_(deref(other.thisptr))

  def diff(self, PyGCZDDLocal other):
    self.thisptr.diff(deref(other.thisptr))

  def intersection(self, PyGCZDDLocal other):
    self.thisptr.intersection(deref(other.thisptr))

  def setOne(self):
    self.thisptr.setOne()

  def setZero(self):
    self.thisptr.setZero()

  def setVariable(self,int v):
    self.thisptr.setVariable(v)

  def saveImage(self, format_, filename):
    self.thisptr.saveImage(format_, filename)

  def reallocate(self):
    self.thisptr.reallocate()

  def size(self):
    return self.thisptr.size()
