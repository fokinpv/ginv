
class ZDD:
  length = 0
  cache = {}

  class __Node:
    def __init__(self, var, m, a):
      assert var >= 0 or (var == -1 and m == None and a == None)
      self.variable = var
      self.mult = m
      self.add = a
      self.degree = 0 if var < 0 else max(self.mult.degree + 1, self.add.degree)

    def __eq__(self, other):
      return id(self) == id(other)

  __one = __Node(-1, None, None)
  __zero = __Node(-1, None, None)

  @staticmethod
  def __createNode(var, m, a):
    assert var >= 0
    degree = max(m.degree + 1, a.degree)
    if (var, degree) not in ZDD.cache:
      ZDD.length += 1
      r = ZDD.__Node(var, m, a)
      ZDD.cache[var, degree] = [r]
    else:
      r = None
      for i in ZDD.cache[var, degree]:
        if i.mult == m and i.add == a:
          r = i
          break
      if not r:
        ZDD.length += 1
        r = ZDD.__Node(var, m, a)
        ZDD.cache[var, degree].append(r)
    return r

  @staticmethod
  def __copy(i):
    if i.variable < 0:
      r = i
    else:
      r = ZDD.__createNode(i.variable, ZDD.__copy(i.mult), ZDD.__copy(i.add))
    return r

  def __add(self, i, j):
    if i.variable > j.variable:
      r = ZDD.__createNode(i.variable, ZDD.__copy(i.mult), self.__add(i.add, j))
    elif i.variable < j.variable:
      r = ZDD.__createNode(j.variable, ZDD.__copy(j.mult), self.__add(j.add, i))
    elif i.variable < 0:
      r = ZDD.__zero if i == j else ZDD.__one
    else:
      m, a = self.__add(i.mult, j.mult), self.__add(i.add, j.add)
      r = a if m == ZDD.__zero else ZDD.__createNode(i.variable, m, a)
    return r

  def __mul(self, i, var):
    if i.variable > var:
      m, a = self.__mul(i.mult, var), self.__mul(i.add, var)
      r = a if m == ZDD.__zero else ZDD.__createNode(i.variable, m, a)
    elif i.variable == var:
      m = self.__add(i.mult, i.add)
      r = ZDD.__zero if m == ZDD.__zero else ZDD.__createNode(var, m, ZDD.__zero)
    elif i.variable >= 0:
      r = ZDD.__createNode(var, ZDD.__copy(i),  ZDD.__zero)
    else:
      r = ZDD.__zero if i == ZDD.__zero else ZDD.__createNode(var, ZDD.__one, ZDD.__zero)
    return r

  def __init__(self, var=-1):
    self.root = ZDD.__zero if var < 0 else ZDD.__createNode(var, ZDD.__one, ZDD.__zero)

  @staticmethod
  def GC(lst):
    cache = ZDD.cache
    ZDD.length = 0
    ZDD.cache = {}
    for i in range(len(lst)):
      lst[i] = ZDD.__copy(lst[i].root)

  def __nonzero__(self):
    return self.root != ZDD.__zero

  def isZero(self):
    return self.root == ZDD.__zero

  def isOne(self):
    return self.root == ZDD.__one

  def isVariable(self):
    return self.root.mult == ZDD.__one and self.root.add == ZDD.__zero

  def setZero(self):
    self.root = ZDD.__zero

  def setOne(self):
    self.root = ZDD.__one

  def __add__(self, other):
    r = ZDD()
    if other == 1:
      r.root = r.__add(self.root, ZDD.__one)
    else:
      r.root = r.__add(self.root, other.root)
    return r

  def __mul__(self, other):
    if self.isOne():
      r = other
    elif self.isZero():
      r = ZDD()
    elif other.isOne():
      r = self
    elif other.isZero():
      r = ZDD()
    elif other.isVariable():
      r = ZDD()
      r.root = r.__mul(self.root, other.root.variable)
    elif self.isVariable():
      r = ZDD()
      r.root = r.__mul(other.root, self.root.variable)
    else:
      assert False
    return r

  def __iter__(self):
    def lex(zdd):
      if zdd.root == ZDD.__zero:
        yield None
      elif zdd.root == ZDD.__one:
        yield []
      else:
        i, p = zdd.root, []
        while i != ZDD.__one:
          p.append([i, True])
          i = i.mult
        yield [j.variable for j, a in p]
        while True:
          while p[-1][0].add == ZDD.__zero or not p[-1][1]:
            del p[-1]
            if not p:
              return
          p[-1][1] = False
          i = p[-1][0].add
          while i != ZDD.__one:
            p.append([i, True])
            i = i.mult
          yield [j.variable for j, a in p if a]

    def compare(m1, m2):
      if len(m1) > len(m2):
        return -1
      elif len(m1) < len(m2):
        return 1
      else:
        for i in range(len(m1)-1, -1, -1):
          if m1[i] > m2[i]:
            return 1
          elif m1[i] > m2[i]:
            return -1
        return 0

    #return sorted(list(lex(self)),  cmp=compare)
    r  = list(lex(self))
    r.sort(cmp=compare)
    return iter(r)

    #if self.root == ZDD.__zero:
      #yield None
    #elif self.root == ZDD.__one:
      #yield []
    #else:
      #i, p = self.root, []
      #while i != ZDD.__one:
        #if (i.mult.degree + 1) <= i.add.degree:
          #p.append([i, False])
          #i = i.add
        #else:
          #p.append([i, True])
          #i = i.mult
      #yield [j.variable for j, a in p if a][::-1]
      #while True:
        #while (p[-1][0].mult.degree + 1) != p[-1][0].add.degree or p[-1][1]:
          #del p[-1]
          #if not p:
            #return
        #p[-1][1] = True
        #i = p[-1][0].mult
        #while i != ZDD.__one:
          #if (i.mult.degree + 1) <= i.add.degree:
            #p.append([i, False])
            #i = i.add
          #else:
            #p.append([i, True])
            #i = i.mult
        #yield [j.variable for j, a in p if a][::-1]

  @staticmethod
  def saveImage(file_format, filename, options={}):
    try:
      import gv
    except Exception:
      print "install the 'libgv-python' for drawing diagrams using the 'graphviz' http://www.graphviz.org/"
      return

    g = gv.graph('ZDD')

    for i in ZDD.cache.itervalues():
      for j in i:
        r = gv.node(g, str(id(j)))
        #gv.setv(r, 'label', '%d#%d' % (j.variable, j.degree))
        gv.setv(r, 'label', '%d' % j.variable)

        if j.mult.variable >= 0:
          m = gv.node(g, str(id(j.mult)))
          #gv.setv(m, 'label', '%d#%d' % (j.mult.variable, j.mult.degree))
          gv.setv(m, 'label', '%d' % j.mult.variable)
        else:
          assert j.mult == ZDD.__one
          m = gv.node(g, '1')
          gv.setv(m, 'shape', 'box')

        if j.add.variable >= 0:
          a = gv.node(g, str(id(j.add)))
          #gv.setv(a, 'label', '%d#%d' % (j.add.variable, j.add.degree))
          gv.setv(a, 'label', '%d' % j.add.variable)
        elif j.add == ZDD.__one:
          a = gv.node(g, '1')
          gv.setv(a, 'shape', 'box')
        else:
          a = gv.node(g, '0')
          gv.setv(a, 'shape', 'box')

        e = gv.edge(r, m)
        gv.setv(e, 'style', 'solid')
        gv.setv(e, 'dir', 'forward')

        e = gv.edge(r, a);
        gv.setv(e, 'style', 'dashed')
        gv.setv(e, 'dir', 'forward')

    for k, v in options.iteritems():
      gv.setv(g, k, v)
    gv.layout(g, 'dot')
    gv.render(g, file_format, filename)


if __name__ == "__main__":
  n, d = 7, 3
  from itertools import *
  var = [ZDD(i) for i in range(n, 0, -1)]
  p = combinations(var, d)
  tail = 1
  for i in range(d-1, 0, -1):
    tail = reduce(lambda x, y: x+y, (reduce(lambda x, y: x*y, m) \
                  for m in combinations(var, i))) + tail
  lst = [reduce(lambda x, y: x*y, m) + tail for m in p]

  def pretty(m):
    if not m:
      return '1'
    else:
      return ''.join('x_%d' % d for d in m)

  for i in range(len(lst)):
    print i
    print ' + '.join(pretty(m) for m in lst[i])

  ZDD.GC(lst)
  ZDD.saveImage('pdf', 'test_deg.pdf')
  print ZDD.length

