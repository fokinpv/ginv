#include <cstdlib>
#include <graphviz/gvc.h>

#include "util/rbtree.h"

inline int compare(int a, int b) {
  if (a > b)
    return 1;
  else if (a < b)
    return -1;
  else
    return 0;
}

using namespace GInv;

int main(int argc, char *argv[]) {
  Allocator allocator[1];
  RBTree<int, compare> a(allocator);

//   int data[]={1, 6, 8, 11, 13, 15, 17, 22, 25, 27};
  int data[]={13, 8, 17, 1, 11, 15, 25, 6, 22, 27};
//   int data[]={27, 25, 22, 17, 15, 13, 11, 8, 6, 1};
  for(int i=0; i < 10; i++)
    a.insert(data[i]);

//   for(int i=0; i < 100; i++)
//     a.insert((3*i+7)%101);

  a.draw("pdf", "rbtree.pdf");

  return EXIT_SUCCESS;
}
