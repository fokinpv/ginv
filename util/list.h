/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A.                                  *
 *   BlinkovUA@info.sgu.ru                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_UTIL_LIST_H
#define GINV_UTIL_LIST_H

#include "config.h"
#include "allocator.h"

namespace GInv {

template <typename T>
class List {
  struct Node {
    T       mData;
    Node*   mNext;

    Node(T data, Node* next):
        mData(data),
        mNext(next) {
    }
    ~Node() {}
  };

  Allocator*  mAllocator;
  Node*       mHead;
  size_t      mLength;

public:
  class Iterator;
  friend class Iterator;

  class ConstIterator {
    friend class Iterator;
    Node* mLink;

  public:
    inline ConstIterator(): mLink(NULL) {}
    inline ConstIterator(const ConstIterator& a): mLink(a.mLink) {}
    inline ConstIterator(Node* node): mLink(node) {}
    inline ~ConstIterator() {}

    inline void operator=(const ConstIterator& a) { mLink = a.mLink; }

    inline operator bool() const { return mLink != NULL; }
    inline bool operator!=(const ConstIterator& a) { return mLink != a.mLink; }
    inline bool operator==(const ConstIterator& a) { return mLink == a.mLink; }
    inline void operator++() {
      assert(mLink != NULL);
      mLink = mLink->mNext;
    }

    inline T& data() const { assert(mLink); return mLink->mData; }
  };

  class Iterator {
    List*      mList;
    Node**     mLink;

  public:
    inline Iterator():
        mList(NULL),
        mLink(NULL) {
    }
    inline Iterator(const Iterator& a):
        mList(a.mList),
        mLink(a.mLink) {
    }
    inline Iterator(List* list):
        mList(list),
        mLink(&mList->mHead) {
    }
    inline ~Iterator() {}

    inline operator ConstIterator() {
      assert(mLink);
      return *mLink;
    }

    inline void operator=(const Iterator& a) {
      mList = a.mList;
      mLink = a.mLink;
    }

    inline operator bool() const { return mLink != NULL && *mLink != NULL; }
    inline bool operator!=(const Iterator& a) { return mLink != a.mLink; }
    inline bool operator==(const Iterator& a) { return mLink == a.mLink; }
    inline void operator++() {
      assert(mLink != NULL);
      mLink = &(*mLink)->mNext;
    }

    inline T& data() const {
      assert(mLink);
      return (*mLink)->mData;
    }

    inline void insert(T data) {
      assert(mLink);
      *mLink = new(mList->mAllocator) Node(data, *mLink);
      ++mList->mLength;
    }
    inline void insert(ConstIterator j) {
      assert(mLink);
      j.mLink->mNext = *mLink;
      *mLink = j.mLink;
      ++mList->mLength;
    }
    inline ConstIterator get() {
      assert(mLink);
      ConstIterator j(*mLink);
      *mLink = (*mLink)->mNext;
      j.mLink->mNext = NULL;
      --mList->mLength;
      return j;
    }
    inline void del() {
      assert(mLink);
      Node* tmp=*mLink;
      *mLink = tmp->mNext;
      mList->mAllocator->destroy(tmp);
      --mList->mLength;
    }
    void clear() {
      assert(mLink);
      while (*mLink)
        del();
      if (mList->mLength)
        std::cout << mList->mLength << std::endl;
      assert(mList->mLength == 0);
    }
  };

  inline List(Allocator *allocator):
      mAllocator(allocator),
      mHead(NULL),
      mLength(0) {
  }
  List(const List& a, Allocator *allocator):
      mAllocator(allocator),
      mHead(NULL),
      mLength(0) {
    Iterator i(this);
    ConstIterator ia(a.mHead);
    while(ia) {
      i.insert(ia.data());
      ++ia;
      ++i;
    }
  }
  inline ~List() { Iterator(this).clear(); }

  inline void clear() { Iterator(this).clear(); }

  inline void swap(List &a) {
    Allocator *tmp1=mAllocator;
    mAllocator = a.mAllocator;
    a.mAllocator = tmp1;

    Node* tmp2 = mHead;
    mHead = a.mHead;
    a.mHead = tmp2;

    size_t tmp3 = mLength;
    mLength = a.mLength;
    a.mLength = tmp3;
  }

  inline void operator=(const List &a) {
    assert(this != &a);
    Iterator(this).clear();
    Iterator i(this);
    ConstIterator ia(a.mHead);
    while(ia) {
      i.insert(ia.data());
      ++ia;
      ++i;
    }
  }

  inline operator bool() const { return mHead != NULL; }
  inline size_t length() const { return mLength; }

  inline ConstIterator begin() const { return mHead; }
  inline Iterator begin() { return this; }

  inline T head() const { assert(mHead); return mHead->mData; }

  inline void push(T data) {
    mHead = new(mAllocator) Node(data, mHead);
    ++mLength;
  }
  inline T pop() {
    assert(mHead);
    Node* tmp=mHead;
    mHead = mHead->mNext;
    T r=tmp->mData;
    mAllocator->destroy(tmp);
    --mLength;
    return r;
  }
};

}

#endif // GINV_UTIL_LIST_H
