/***************************************************************************
 *   Copyright (C) 2009 by Blinkov Yu. A.                                  *
 *   BlinkovUA@info.sgu.ru                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <gmp.h>

#include "./allocator.h"

namespace GInv {

class Integer {
protected:
  Integer(const Integer &) {}
  void operator=(const Integer& ) {}

  mpz_t mMpz_t;

public:
  void* operator new(size_t n, Allocator& allocator) { return allocator.allocate(n); }
  void operator delete(void *) {}

  Integer();
  Integer(const Integer &a, Allocator& allocator);
  ~Integer();

  void clear(Allocator& allocator);
  void swap(Integer& a, Allocator& allocator);
  void set(const Integer& a, Allocator& allocator);
  
  bool isZero() const { return mpz_sgn(mMpz_t) == 0; }
  void setZero() { mpz_sgn(mMpz_t) == 0; }
  bool isOne() const { return mpz_cmp_ui(mMpz_t, 1ul) == 0; }
  bool isAbsOne() const { return  mpz_cmpabs_ui(mMpz_t, 1ul) == 0; }
  void setOne(Allocator& allocator);

  void add(const Integer& a, const Integer& b, Allocator& allocator);
  void sub(const Integer& a, const Integer& b, Allocator& allocator);
  void mult(const Integer& a, const Integer& b, Allocator& allocator);
  bool divisible(const Integer& a) const;
  void div(const Integer& a, const Integer& b, Allocator& allocator);
  void mod(const Integer& a, const Integer& b, Allocator& allocator);
  void gcd(const Integer& a, const Integer& b, Allocator& allocator);
  void pow(const Integer& a, unsigned long n, Allocator& allocator);

  void read(std::istream &in, Allocator& allocator);
  void write(std::ostream &out, Allocator& allocator) const;
};

}