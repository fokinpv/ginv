/***************************************************************************
 *   Copyright (C) 2009 by Blinkov Yu. A.                                  *
 *   BlinkovUA@info.sgu.ru                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "./integer.h"

namespace GInv {

Integer::Integer() {
  mpz_init(mMpz_t);
}

inline int abs(int a) {
  return (a >= 0)? a: -a;
}

inline int max(int a, int b) {
  return (a >= b)? a: b;
}

Integer::Integer(const Integer &a, Allocator& allocator) {
  set(a, allocator);
}

Integer::~Integer() {
  assert(mMpz_t->_mp_alloc == 0);
}

void Integer::clear(Allocator& allocator) {
  allocator.deallocate(mMpz_t->_mp_d, mMpz_t->_mp_alloc*sizeof(mp_limb_t));
  mMpz_t->_mp_size = 0;
  mMpz_t->_mp_alloc = 0;
}

void Integer::swap(Integer& a, Allocator& allocator) {
  assert(this != &a);
  mpz_swap(mMpz_t, a.mMpz_t);
}

void Integer::set(const Integer& a, Allocator& allocator) {
  int s = abs(a.mMpz_t->_mp_size);
  if (mMpz_t->_mp_alloc < s) {
    allocator.deallocate(mMpz_t->_mp_d, mMpz_t->_mp_alloc*sizeof(mp_limb_t));
    mMpz_t->_mp_alloc = s;
    mMpz_t->_mp_d = (mp_limb_t*)allocator.allocate(mMpz_t->_mp_alloc*sizeof(mp_limb_t));
  }
  mpz_set(mMpz_t, a.mMpz_t);
}

void Integer::setOne(Allocator& allocator) {
  if (mMpz_t->_mp_alloc < 1) {
    mMpz_t->_mp_alloc = 1;
    mMpz_t->_mp_d = (mp_limb_t*)allocator.allocate(mMpz_t->_mp_alloc*sizeof(mp_limb_t));
  }
  mpz_set_ui(mMpz_t, 1ul);
}

void Integer::add(const Integer& a, const Integer& b, Allocator& allocator) {
  assert(this != &a);
  assert(this != &b);
  int s = max(abs(a.mMpz_t->_mp_size), abs(b.mMpz_t->_mp_size)) + 1;
  if (mMpz_t->_mp_alloc < s) {
    mMpz_t->_mp_alloc = s;
    mMpz_t->_mp_d = (mp_limb_t*)allocator.allocate(mMpz_t->_mp_alloc*sizeof(mp_limb_t));
  }
  mpz_add(mMpz_t, a.mMpz_t, b.mMpz_t);
}

void Integer::sub(const Integer& a, const Integer& b, Allocator& allocator) {
  assert(this != &a);
  assert(this != &b);
  int s = max(abs(a.mMpz_t->_mp_size), abs(b.mMpz_t->_mp_size)) + 1;
  if (mMpz_t->_mp_alloc < s) {
    mMpz_t->_mp_alloc = s;
    mMpz_t->_mp_d = (mp_limb_t*)allocator.allocate(mMpz_t->_mp_alloc*sizeof(mp_limb_t));
  }
  mpz_sub(mMpz_t, a.mMpz_t, b.mMpz_t);
}

}
