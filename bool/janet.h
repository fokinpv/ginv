/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_JANET_H
#define GINV_BOOL_JANET_H

#include "util/list.h"
#include "util/rbtree.h"

#include "config.h"

#ifdef GINV_BOOL_GRAPHVIZ
  #include <graphviz/gvc.h>
  #include <cstdio>
#endif // GINV_BOOL_GRAPHVIZ

#include "monom.h"
#include "poly.h"
#include "variables.h"

namespace GInv {

class BoolTQ;
class BoolJanet {
  struct Node {
    BoolMonom::Variable mVariable;
    Node*               mDepth;
    Node*               mBreadth;
    BoolPoly*           mWrap;
    bool*               mProlong; 

    Node(BoolMonom::Variable variable, BoolPoly* wrap=NULL):
        mVariable(variable),
        mDepth(NULL),
        mBreadth(NULL),
        mWrap(wrap),
        mProlong(NULL) {
    }
    ~Node() {}
  };

  static void copy(Node **node1, const Node *node2, Allocator* allocator);
  static void clear(Node *node, Allocator* allocator);
  static void clearAll(Node *node, Allocator* allocator);
  static void remove(Node **node, Allocator* allocator, BoolTQ& tq);
  static void remove(Node **node, const BoolMonom *m, int v, Allocator* allocator, BoolTQ& tq);
  
  static void border(Node *node, BoolMonom::Variable variable, BoolTQ& tq);
  static void border(Node *node, BoolTQ& tq);
  static void borderProlong(Node *node, BoolMonom::Variable variable, BoolTQ& tq);
  static void borderProlong(Node *node, BoolTQ& tq);
  
  static void prolong(Node *node, BoolMonom::Variable variable, BoolTQ& tq);
  static void prolong(Node *node, BoolTQ& tq);
  static void prolong1(Node *node, BoolMonom::Variable variable, int deg, BoolTQ& tq);
  static void prolong1(Node *node, int deg,  BoolTQ& tq);
  static void print(const Node *node, std::ostream& out);
  static int length(const Node *node);
  static bool divisible(const Node *node, const BoolMonom *m);
  static bool valid(const Node *node);

  template <typename D> static void nfTail(const Node *node, const D& a) {
  assert(node);
    do {
      if (node->mDepth)
        nfTail(node->mDepth, a);
      else {
        assert(node->mWrap);
        node->mWrap->nfTail(a);
        node->mWrap->reallocate();
      }
      node = node->mBreadth;
    } while(node);
  }    
  
  
#ifdef GINV_BOOL_GRAPHVIZ
  static void saveImage(const Node *node, Agnode_t *t, Agraph_t *g);
#endif // GINV_BOOL_GRAPHVIZ

  Allocator*  mAllocator;
  Node*       mRoot;

public:
  class ConstIterator {
    const BoolJanet::Node* mLink;

    ConstIterator(const BoolJanet::Node* node):
        mLink(node) {
    }

  public:
    ConstIterator():
        mLink(NULL) {
    }
    ConstIterator(const BoolJanet& a):
        mLink(a.mRoot) {
    }
    ~ConstIterator() {}

    operator bool() const { return mLink; }
    BoolMonom::Variable variable() const {
      assert(mLink != NULL);
      return mLink->mVariable;
    }
    ConstIterator depth() const {
      assert(mLink != NULL);
      return mLink->mDepth;
    }
    ConstIterator breadth() const {
      assert(mLink != NULL);
      return mLink->mBreadth;
    }
    BoolPoly* poly() const {
      assert(mLink != NULL);
      return mLink->mWrap;
    }

    friend bool operator ==(const ConstIterator& a, const ConstIterator& b) {
      return a.mLink == b.mLink;
    }
    friend bool operator !=(const ConstIterator& a, const ConstIterator& b) {
      return a.mLink != b.mLink;
    }
  };
  
  static int mSize;
  
  BoolJanet(Allocator* allocator):
      mAllocator(allocator),
      mRoot(NULL) {
  }
  BoolJanet(const BoolJanet &a, Allocator* allocator):
      mAllocator(allocator),
      mRoot(NULL) {
    if (a.mRoot)
      copy(&mRoot, a.mRoot, mAllocator);
    assert(assertValid());
  }
  ~BoolJanet() {
    if (mRoot)
      clear(mRoot, mAllocator);
  }

  void clearAll() {
    if (mRoot) {
      clearAll(mRoot, mAllocator);
      mRoot = NULL;
    }
  }

  void swap(BoolJanet &a) {
    Allocator *tmp1=mAllocator;
    mAllocator = a.mAllocator;
    a.mAllocator = tmp1;

    Node* tmp2=mRoot;
    mRoot = a.mRoot;
    a.mRoot = tmp2;
  }

  inline operator bool() const { return mRoot; }
  inline bool isLinear() const {
    return mRoot &&  mRoot->mDepth == NULL && mRoot->mBreadth == NULL;
  }
  inline int length() const { return (mRoot) ? length(mRoot): 0; }
  BoolPoly* find(const BoolMonom* m) const;

  void insert(BoolPoly* wrap, BoolTQ& tq, bool prl=false);
  
  template <typename D> void nfTail(const D& a) {
    if (mRoot)
      nfTail(mRoot, a);
  }

  void border(BoolTQ& tq) {
    if (mRoot)
      border(mRoot, tq);
  }
  void border(BoolMonom::Variable variable, BoolTQ& tq) {
    if (mRoot)
      border(mRoot, variable, tq);
  }
  
  void borderProlong(BoolTQ& tq) {
    if (mRoot)
      borderProlong(mRoot, tq);
  }
  void borderProlong(BoolMonom::Variable variable, BoolTQ& tq) {
    if (mRoot)
      borderProlong(mRoot, variable, tq);
  }
  
  void prolong(BoolMonom::Variable variable, BoolTQ& tq) {
    if (mRoot)
      prolong(mRoot, variable, tq);
  }
  void prolong(BoolTQ& tq) {
    if (mRoot)
      prolong(mRoot, tq);
  }
  
  void prolong(BoolMonom::Variable variable, int deg, BoolTQ& tq) {
    if (mRoot)
      prolong1(mRoot, variable, deg, tq);
  }
  void prolong(BoolTQ& tq, int deg) {
    if (mRoot)
      prolong1(mRoot, deg, tq);
  }
  
  void remove(BoolPoly* wrap);
  bool remove(const BoolMonom *m, BoolTQ& tq) {
    int l=length();
    if (mRoot && mRoot->mVariable >= m->variable(0))
      remove(&mRoot, m, 0, mAllocator, tq);
    assert(mRoot == NULL || !divisible(mRoot, m));
    return l != length();
  }
  void remove(BoolTQ& tq) {
    if (mRoot)
      remove(&mRoot, mAllocator, tq);
    assert(mRoot == NULL);
  }

#ifdef GINV_BOOL_GRAPHVIZ
  void saveImage(const char* format, const char* filename) const;
#endif // GINV_BOOL_GRAPHVIZ

  friend std::ostream& operator<<(std::ostream& out, const BoolJanet &a) {
    if (a.mRoot)
      print(a.mRoot, out);
    return out;
  }

  bool assertValid() const;
};

typedef GC<BoolJanet> GCBoolJanet;

}

#endif // GINV_BOOL_JANET_H
