/***************************************************************************
 *   Copyright (C) 2010 by Blinkov Yu. A.                                  *
 *   BlinkovUA@info.sgu.ru                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cstdlib>
#include <fstream>

#include "tq.h"

using namespace GInv;

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "ginv number_variables <file>" << std::endl;
    return EXIT_FAILURE;
  }

  int vars=atoi(argv[1]);
  if (vars <= 0) {
    std::cerr << "number_variables " << vars << " <= 0" << std::endl;
    return EXIT_FAILURE;
  }

  std::ifstream in(argv[2]);
  if (!in.is_open()) {
    std::cerr << "Unable to open file '" << argv[2] << "'" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "number_variables - " << vars << ", file - '" << argv[2] << "'" << std::endl;

  BoolTQ tq(vars);
  while(!(in >> std::ws).eof()) {
    BoolPoly *p=new BoolPoly;
    const int dim=1024;
    int var[dim], n=0;
    while((in >> std::ws).peek() != ';') {
      char i=in.get();
      std::cout << i;
      if (i == 'x') {
        assert(n + 1 < dim);
        int v=0;
        in >> v;
        std::cout << v;
        var[n++] = vars - v;
      }
      else if (i == '*') {
      }
      else if (i == '+') {
        p->add(var, n);
        n = 0;
      }
      else  if (i == '1') {
      }
    }
    p->add(var, n);
    in.get();
    in >> std::ws;
    p->canonicalize();
    std::cout << " -> " << *p << std::endl;
    tq.insert(p);
    in >> std::ws;
  }
  in.close();

  tq.build();

  return EXIT_SUCCESS;
}
