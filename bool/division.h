/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_DIVISION_H
#define GINV_BOOL_DIVISION_H

#include "config.h"

#if defined(GINV_BOOL_DIVISION_ALEX)
#include "alex.h"
#endif // defined(GINV_BOOL_DIVISION_ALEX)

#if defined(GINV_BOOL_DIVISION_JANET)
#include "janet.h"
#endif // defined(GINV_BOOL_DIVISION_JANET)

namespace GInv {
  typedef GINV_BOOL_DIVISION BoolDivision;
}

#endif // GINV_BOOL_DIVISION_H
