/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_ZDDLOCAL_H
#define GINV_BOOL_ZDDLOCAL_H

#include "zdd/local.h"

#include "monom.h"

namespace GInv {

// class BoolZDDLocal {
//   Allocator*    mAllocator;
//   GCZDDLocal    mZDD;
//   bool          mUpdateLm;
//   BoolMonom*    mLm;
//
// public:
//   class ConstIterator {
//     Allocator*          mAllocator;
//     BoolMonom*          mMonom;
//     const BoolZDDLocal* mZDD;
//
//   public:
//     inline ConstIterator():
//         mAllocator(NULL),
//         mMonom(NULL),
//         mZDD(NULL) {
//     }
//     inline ConstIterator(const ConstIterator& a):
//         mMonom(NULL),
//         mZDD(&a) {
//     }
//     inline ~ConstIterator() {}
//
//     inline void operator=(const ConstIterator& a) { mIterator = a.mIterator; }
//
//     inline operator bool() const { return mIterator.operator bool(); }
//     inline bool operator!=(const ConstIterator& a) { mIterator != a.mIterator; }
//     inline bool operator==(const ConstIterator& a) { mIterator == a.mIterator; }
//
//
//     inline void operator++() {  mIterator.operator++(); }
//
//     inline const BoolMonom* monom() const { return mIterator.data(); }
//   };
//
//   BoolZDDLocal(Allocator* mAllocator);
//
// };

}

#endif // GINV_BOOL_ZDDLOCAL_H
