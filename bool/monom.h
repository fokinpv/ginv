/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_MONOM_H
#define GINV_BOOL_MONOM_H

#include "util/allocator.h"
#include "config.h"

namespace GInv {

// class BoolMonom {
// public:
//   typedef int Variable;
//
// protected:
//   int         mSize;
//   int         mDegree;
//   Variable*   mVariables;
//
// public:
//   BoolMonom(int *a, int n):
//       mSize(n),
//       mDegree(0),
//       mVariables(a) {
//     assert(n == 0 || a == NULL);
//   }
//   ~BoolMonom() {}
//
//   void swap(BoolMonom& a);
//   void operator=(const BoolMonom& a);
//
//   operator bool() const { return mDegree > 0; }
//   bool isZero() const { return mDegree == 0; }
//
//   int degree() const { return mDegree; }
//   Variable operator[](int i) const {
//     assert(0 <= i && i < mDegree);
//     return mVariables[i];
//   }
//   Variable variable(int i) const {
//     assert(0 <= i && i < mDegree);
//     return mVariables[i];
//   }
// };

class BoolMonom {
public:
  typedef int Variable;

protected:
  Allocator*  mAllocator;
  int         mSize;
  int         mDegree;
  Variable*   mVariables;

public:
  explicit BoolMonom(Allocator* allocator):
      mAllocator(allocator),
      mSize(0),
      mDegree(0),
      mVariables(NULL) {
  }
  BoolMonom(const BoolMonom& a, Allocator* allocator);
  ~BoolMonom() {
    if (mSize)
      mAllocator->dealloc(mVariables, mSize);
  }
  BoolMonom(Variable var, Allocator* allocator);
  BoolMonom(const BoolMonom& a, Variable var, Allocator* allocator);
  BoolMonom(const BoolMonom& a, const BoolMonom& b, Allocator* allocator);
  BoolMonom(int *var, int n, Allocator* allocator);

  void swap(BoolMonom& a);
  void operator=(const BoolMonom& a);

  operator bool() const { return mDegree > 0; }
  bool isZero() const { return mDegree == 0; }

  int degree() const { return mDegree; }
  Variable operator[](int i) const {
    assert(0 <= i && i < mDegree);
    return mVariables[i];
  }
  Variable variable(int i) const {
    assert(0 <= i && i < mDegree);
    return mVariables[i];
  }
  Variable first() const {
    assert(mDegree > 0);
    return mVariables[0];
  }
  Variable last() const {
    assert(mDegree > 0);
    return mVariables[mDegree - 1];
  }
  
  bool divisible(Variable var) const;
  bool divisible(const BoolMonom& a) const;

  void div(const BoolMonom& a, Variable var);
  void div(const BoolMonom& a, const BoolMonom& b);

  int alex(const BoolMonom& a) const;
  int drl(const BoolMonom& a) const;
  int lex(const BoolMonom& a) const;

  static inline int compare(const BoolMonom* a, const BoolMonom* b) {
    return a->GINV_BOOL_ORDER(*b);
  }

  int lcm(const BoolMonom& a) const;
  int gcd(const BoolMonom& a) const;

  bool assertValid() const;
};

std::ostream& operator<<(std::ostream& out, const BoolMonom &a);

typedef GC<BoolMonom> GCBoolMonom;

}

#endif // GINV_BOOL_MONOM_H
