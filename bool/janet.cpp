/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.t,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "janet.h"
#include "tq.h"

namespace GInv {

int BoolJanet::mSize=0;
  
void BoolJanet::copy(Node **node1, const Node *node2, Allocator* allocator) {
  assert(node2);
  do {
    *node1 = new(allocator) BoolJanet::Node(node2->mVariable, node2->mWrap);
    if (node2->mProlong) {
      (*node1)->mProlong = new(allocator) bool[BoolJanet::mSize];
      for(int i=0; i < BoolJanet::mSize; i++)
        (*node1)->mProlong[i] = node2->mProlong[i];
    }
    if (node2->mDepth)
      copy(&(*node1)->mDepth, node2->mDepth, allocator);
    node1 = &(*node1)->mBreadth;
    node2 = node2->mBreadth;
  } while(node2);
}

void BoolJanet::clear(Node *node, Allocator* allocator) {
  assert(node);
  do {
    if (node->mDepth)
      clear(node->mDepth, allocator);
    Node *tmp=node;
    node = node->mBreadth;
    if (tmp->mProlong)
      allocator->dealloc(tmp->mProlong, BoolJanet::mSize);
    allocator->destroy(tmp);
  } while(node);
}

void BoolJanet::clearAll(Node *node, Allocator* allocator) {
  assert(node);
  do {
    if (node->mDepth)
      clear(node->mDepth, allocator);
    Node *tmp=node;
    node = node->mBreadth;
    if (tmp->mWrap)
      delete tmp->mWrap;
    if (tmp->mProlong)
      allocator->dealloc(tmp->mProlong, BoolJanet::mSize);
    allocator->destroy(tmp);
  } while(node);
}

void BoolJanet::border(Node *node, BoolMonom::Variable variable, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      border(node->mDepth, variable, tq);
    else {
      assert(node->mWrap);
      assert(node->mProlong);
      if (!node->mProlong[variable]) {
        if (tq.mBorderDegree < 0 || tq.mBorderDegree > node->mWrap->lm()->degree()) {
          tq.mBorderDegree = node->mWrap->lm()->degree();
          tq.mBorderVariable = node->mWrap->lm()->last();
        }
        else if (tq.mBorderDegree == node->mWrap->lm()->degree() && 
                 tq.mBorderVariable > node->mWrap->lm()->last()) {
          tq.mBorderVariable = node->mWrap->lm()->last();
        }
      }
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::border(Node *node, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      border(node->mDepth, tq);
    else {
      assert(node->mWrap);
      for(int v=0; v < node->mWrap->lm()->degree(); v++) {
        if (!node->mProlong[node->mWrap->lm()->variable(v)]) {
          int variable=node->mWrap->lm()->variable(v);
          if (tq.mBorderDegree < 0 || tq.mBorderDegree > node->mWrap->lm()->degree()) {
            tq.mBorderDegree = node->mWrap->lm()->degree();
            tq.mBorderVariable = node->mWrap->lm()->last();
          }
          else if (tq.mBorderDegree == node->mWrap->lm()->degree() && 
                   tq.mBorderVariable > node->mWrap->lm()->last()) {
            tq.mBorderVariable = node->mWrap->lm()->last();
          }
        }
      }
    }
    if (node->mBreadth) {
      border(node->mBreadth, node->mVariable, tq);
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::borderProlong(Node *node, BoolMonom::Variable variable, BoolTQ& tq) {
  assert(node);
  do {
    if (tq.Break())
      break;
    if (node->mDepth)
      borderProlong(node->mDepth, variable, tq);
    else {
      assert(node->mWrap);
      assert(node->mProlong);
      if (!node->mProlong[variable]) {
        if (tq.mBorderDegree == node->mWrap->lm()->degree() && 
            tq.mBorderVariable == node->mWrap->lm()->last()) {
          if (!node->mWrap->isProlong() ||
              node->mWrap->lm()->first() != node->mWrap->mAncestor.first()) {
            BoolPoly *p=new BoolPoly;
            p->prolong(*node->mWrap, variable);
            if (!p->isZero())
              p->nf(tq);
            tq.insert(p);
          }
          node->mProlong[variable] = true;
        }
      }
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::borderProlong(Node *node, BoolTQ& tq) {
  assert(node);
  do {
    if (tq.Break())
      break;
    if (node->mDepth)
      borderProlong(node->mDepth, tq);
    else {
      assert(node->mWrap);
      for(int v=0; v < node->mWrap->lm()->degree(); v++) {
        if (!node->mProlong[node->mWrap->lm()->variable(v)]) {
          int variable=node->mWrap->lm()->variable(v);
          if (tq.mBorderDegree == node->mWrap->lm()->degree() && 
              tq.mBorderVariable == node->mWrap->lm()->last()) {
            BoolPoly *p=new BoolPoly;
            p->prolong(*node->mWrap, variable);
//             std::cout << *node->mWrap->lm() << " " << variable << " = " << *p << std::endl;
//             if (!p->isZero())
//               p->nf(tq);
            tq.insert(p);
            node->mProlong[variable] = true;
          }
        }
      }
    }
    if (node->mBreadth) {
      borderProlong(node->mBreadth, node->mVariable, tq);
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::prolong(Node *node, BoolMonom::Variable variable, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      prolong(node->mDepth, variable, tq);
    else {
      assert(node->mWrap);
      assert(node->mProlong);
      if (!node->mProlong[variable]) {
        BoolPoly *p=new BoolPoly;
        p->prolong(*node->mWrap, variable);
//         if (!p->isZero())
//           p->nf(tq);
        tq.insert(p);
        node->mProlong[variable] = true;
      }
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::prolong(Node *node, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      prolong(node->mDepth, tq);
    else {
      assert(node->mWrap);
      for(int v=0; v < node->mWrap->lm()->degree(); v++) {
        if (!node->mProlong[node->mWrap->lm()->variable(v)]) {
          BoolPoly* p=new BoolPoly;
          p->prolong(*node->mWrap, node->mWrap->lm()->variable(v));
          if (!p->isZero())
            p->nf(tq);
          tq.insert(p);
          node->mProlong[node->mWrap->lm()->variable(v)] = true;
        }
      }
    }
    if (node->mBreadth) {
      prolong(node->mBreadth, node->mVariable, tq);
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::prolong1(Node *node, BoolMonom::Variable variable, int deg, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      prolong1(node->mDepth, variable, deg, tq);
    else {
      assert(node->mWrap);
      assert(node->mProlong);
      if (node->mWrap->lm()->degree() == deg && !node->mProlong[variable]) {
        BoolPoly *p=new BoolPoly;
        p->prolong(*node->mWrap, variable);
        if (!p->isZero())
          p->nf(tq);
        tq.insert(p);
        node->mProlong[variable] = true;
      }
      if (tq.Break())
        break;
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::prolong1(Node *node, int deg, BoolTQ& tq) {
  assert(node);
  do {
    if (node->mDepth)
      prolong1(node->mDepth, deg, tq);
    else {
      assert(node->mWrap);
      if (node->mWrap->lm()->degree() == deg) {
        for(int v=0; v < node->mWrap->lm()->degree(); v++) {
          if (!node->mProlong[node->mWrap->lm()->variable(v)]) {
            BoolPoly* p=new BoolPoly;
            p->prolong(*node->mWrap, node->mWrap->lm()->variable(v));
//             if (!p->isZero())
//               p->nf(tq);
            tq.insert(p);
            node->mProlong[node->mWrap->lm()->variable(v)] = true;
          }
        }
        if (tq.Break())
          break;
      }
    }
    if (node->mBreadth) {
      prolong1(node->mBreadth, node->mVariable, deg, tq);
    }
    node = node->mBreadth;
  } while(node);
}

void BoolJanet::remove(Node **node, Allocator* allocator, BoolTQ& tq) {
  assert(*node);
  do {
    if ((*node)->mDepth)
      remove(&(*node)->mDepth, allocator, tq);
    else {
      assert((*node)->mWrap);
      tq.insert((*node)->mWrap);
    }
    assert((*node)->mDepth == NULL);
    Node *tmp=*node;
    *node = (*node) ? (*node)->mBreadth: NULL;
    if (tmp->mProlong)
      allocator->dealloc(tmp->mProlong, BoolJanet::mSize);
    allocator->destroy(tmp);
  } while(*node);
}

void BoolJanet::remove(Node **node, const BoolMonom *m, int v, Allocator* allocator, BoolTQ& tq) {
  do {
    assert(*node);
    assert(0 <= v && v < m->degree());
    assert((*node)->mVariable >= m->variable(v));
    if ((*node)->mDepth) {
      if ((*node)->mVariable > m->variable(v)) {
        if ((*node)->mDepth->mVariable >= m->variable(v))
          remove(&(*node)->mDepth, m, v, allocator, tq);
      }
      else {
        if (v+1 == m->degree())
          remove(&(*node)->mDepth, allocator, tq);
        else {
          if ((*node)->mDepth->mVariable >= m->variable(v+1))
            remove(&(*node)->mDepth, m, v+1, allocator, tq);
        }
      }
    }
    else {
      if ((*node)->mVariable == m->variable(v) && v+1 == m->degree()) {
        tq.insert((*node)->mWrap);
        (*node)->mWrap = NULL;
      }
    }
    if ((*node)->mDepth || (*node)->mWrap)
      node = &(*node)->mBreadth;
    else {
      Node *tmp=*node;
      *node = (*node) ? (*node)->mBreadth: NULL;
      if (tmp->mProlong)
        allocator->dealloc(tmp->mProlong, BoolJanet::mSize);
      allocator->destroy(tmp);
    }
  } while(*node && (*node)->mVariable >= m->variable(v));
}

int BoolJanet::length(const Node *node) {
  assert(node);
  int r=0;
  do {
    if (node->mDepth)
      r += length(node->mDepth);
    else {
      assert(node->mWrap);
      ++r;
    }
    node = node->mBreadth;
  } while(node);
  return r;
}

void BoolJanet::print(const Node *node, std::ostream& out) {
  assert(node);
  do {
    if (node->mDepth)
      print(node->mDepth, out);
    else {
      assert(node->mWrap);
      out << *node->mWrap << std::endl;
//       out << *node->mWrap->lm() << ','
//           << node->mWrap->length() << ',' 
//           << node->mWrap->isProlong() << std::endl;
    }
    node = node->mBreadth;
  } while(node);
}

bool BoolJanet::divisible(const Node *node, const BoolMonom *m) {
  assert(node);
  bool r=false;
  do {
    if (node->mDepth)
      r = r || divisible(node->mDepth, m);
    else {
      assert(node->mWrap);
//       if (!node->mWrap->isProlong() && node->mWrap->lm()->divisible(*m)) {
      if (node->mWrap->lm()->divisible(*m)) {
        r = true;
        std::cout << *node->mWrap->lm() << std::endl;
      }
    }
    node = node->mBreadth;
  } while(node);
  return r;
}


bool BoolJanet::valid(const Node *node) {
  assert(node);
  bool r=true;
  do {
    if (node->mDepth == NULL)
      r = r && node->mWrap != NULL;
    else
      r = r && node->mWrap == NULL && node->mVariable > node->mDepth->mVariable && valid(node->mDepth);

    if (node->mBreadth) {
      r = r && node->mVariable > node->mBreadth->mVariable;
      node = node->mBreadth;
    }
    else {
      r = r && (node->mDepth != NULL || (node->mWrap != NULL && node->mProlong != NULL));
      break;
    }
  } while(r);
  return r;
}

BoolPoly* BoolJanet::find(const BoolMonom* m) const {
  Node *node=mRoot;
  for(int i=0; i < m->degree(); i++) {
    while(node && node->mVariable > m->variable(i))
      node = node->mBreadth;
    if (node == NULL)
      return NULL;
    else if (node->mVariable == m->variable(i)) {
      if (node->mWrap)
        return node->mWrap;
      else
        node = node->mDepth;
    }
  }
  return NULL;
}

void BoolJanet::insert(BoolPoly* wrap, BoolTQ& tq, bool prl) {
  assert(wrap);
  assert(find(wrap->lm()) == NULL);
  const BoolMonom *m=wrap->lm();
  Node **node=&mRoot;
  for(int i=0; ; i++) {
    while(*node && (*node)->mVariable > m->variable(i)) {
      if (prl) {
        BoolPoly *p=new BoolPoly;
        p->prolong(*wrap, (*node)->mVariable);
        tq.insert(p);
      }
      node = &(*node)->mBreadth;
    }
    if (*node == NULL) 
      *node = new(mAllocator) Node(m->variable(i));
    else if ((*node)->mVariable < m->variable(i)) {
      if (prl)
        prolong(*node, m->variable(i), tq);
      Node *tmp = new(mAllocator) Node(m->variable(i));
      tmp->mBreadth = *node;
      *node = tmp;
    }
    if (i + 1 < m->degree())
      node = &(*node)->mDepth;
    else {
      if (prl)
        for(int v=0; v < wrap->lm()->degree(); v++) {
          BoolPoly* p=new BoolPoly;
          p->prolong(*wrap, wrap->lm()->variable(v));
          tq.insert(p);
        }
      (*node)->mWrap = wrap;
      (*node)->mProlong = new(mAllocator) bool[BoolJanet::mSize];
      for(int i=0; i < BoolJanet::mSize; i++)
        (*node)->mProlong[i] = false;
      break;
    }
  }
  assert(find(wrap->lm()) == wrap);
//   if (!assertValid()) {
//     saveImage("pdf", "aaa2.pdf");
//     std::cout << *wrap->lm() << std::endl;
//   }

  assert(assertValid());
}

void BoolJanet::remove(BoolPoly* wrap) {
  assert(find(wrap->lm()) == wrap);
  const BoolMonom *m=wrap->lm();
  Node ***nodes= new(mAllocator) Node**[m->degree()];
  nodes[0] = &mRoot;
  for(int i=0; i < m->degree(); i++) {
    if (i)
      nodes[i] = &(*nodes[i-1])->mDepth;
    assert(*nodes[i]);
    while((*nodes[i])->mVariable > m->variable(i)) {
      nodes[i] = &(*nodes[i])->mBreadth;
      assert(*nodes[i]);
    }
    assert((*nodes[i])->mVariable == m->variable(i));
  }
  int l=m->degree();
  assert((*nodes[l-1])->mWrap == wrap);
  do {
    --l;
    assert(*nodes[l]);
    assert((*nodes[l])->mVariable == m->variable(l));
    Node *tmp = *nodes[l];
    *nodes[l] = (*nodes[l])->mBreadth;
    if (tmp->mProlong)
      mAllocator->dealloc(tmp->mProlong, BoolJanet::mSize);
    mAllocator->destroy(tmp);
    if (*nodes[l]) {
      break;
    }
  } while(l > 0 && *nodes[l] == (*nodes[l-1])->mDepth);
  mAllocator->dealloc(nodes, m->degree());
  assert(find(wrap->lm()) != wrap);
  assert(assertValid());
}

#ifdef GINV_BOOL_GRAPHVIZ
void BoolJanet::saveImage(const Node *node, Agnode_t *t, Agraph_t *g)  {
  assert(node);
  char buffer[256];

  sprintf(buffer, "%p", node);
  Agnode_t *c = agnode(g, buffer);
  sprintf(buffer, "%d", node->mVariable);
  agsafeset(c, (char*)"label", buffer, (char*)"");

  if (t) {
    Agedge_t *e = agedge(g, t, c);
    agsafeset(e, (char*)"style", (char*)"solid", (char*)"");
    agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");
  }

  if (node->mDepth){
    assert(node->mWrap == NULL);
    saveImage(node->mDepth, c, g);
  }
  else {
    assert(node->mWrap);
    agsafeset(c, (char*)"shape", (char*)"box", (char*)"");
    if (!node->mWrap->isProlong())
      agsafeset(c, (char*)"style", (char*)"filled", (char*)"");
  }

  node = node->mBreadth;
  while(node) {
    t = c;

    sprintf(buffer, "%p", node);
    c = agnode(g, buffer);
    sprintf(buffer, "%d", node->mVariable);
    agsafeset(c, (char*)"label", buffer, (char*)"");

    Agedge_t *e = agedge(g, t, c);
    agsafeset(e, (char*)"style", (char*)"dashed", (char*)"");
    agsafeset(e, (char*)"dir", (char*)"forward", (char*)"");

    if (node->mWrap) {
      agsafeset(c, (char*)"shape", (char*)"box", (char*)"");
      if (!node->mWrap->isProlong())
        agsafeset(c, (char*)"style", (char*)"filled", (char*)"");
    }
    else {
      assert(node->mDepth);
      saveImage(node->mDepth, c, g);
    }
    node = node->mBreadth;
  }
}

void BoolJanet::saveImage(const char* format, const char* filename) const {
  GVC_t *gvc=gvContext();
  Agraph_t *g=agopen((char*)"BoolJanet", AGDIGRAPH);
  if (mRoot)
    saveImage(mRoot, NULL, g);
  gvLayout(gvc, g, (char*)"dot");
  gvRenderFilename(gvc, g, format, filename);
  gvFreeLayout(gvc, g);
  agclose(g);
}
#endif // GINV_BOOL_GRAPHVIZ

bool BoolJanet::assertValid() const {
  return mRoot == NULL || valid(mRoot);
}

}
