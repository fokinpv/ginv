/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_LIST_H
#define GINV_BOOL_LIST_H

#include "util/list.h"
#include "monom.h"

namespace GInv {

class BoolList {
  Allocator*              mAllocator;
  List<const BoolMonom*>  mList;
//   BoolMonom               mAncestor;

  inline void clear() {
    while(mList) {
      mAllocator->destroy(mList.pop());
    }
  }
  void add1(BoolList& a);

public:
  BoolMonom               mAncestor;
  const BoolList*         mParent;

  class ConstIterator {
    friend class BoolList;
    List<const BoolMonom*>::ConstIterator mIterator;

  inline ConstIterator(const List<const BoolMonom*>::ConstIterator& a):
        mIterator(a) {}

  public:
    inline ConstIterator():
        mIterator() {}
    inline ConstIterator(const ConstIterator& a):
        mIterator(a.mIterator) {}
    inline ~ConstIterator() {}

    inline void operator=(const ConstIterator& a) { mIterator = a.mIterator; }

    inline operator bool() const { return mIterator.operator bool(); }
    inline bool operator!=(const ConstIterator& a) { mIterator != a.mIterator; }
    inline bool operator==(const ConstIterator& a) { mIterator == a.mIterator; }


    inline void operator++() {  mIterator.operator++(); }

    inline const BoolMonom* monom() const { return mIterator.data(); }
  };

  inline BoolList(Allocator *allocator):
      mAllocator(allocator),
      mList(allocator),
      mAncestor(allocator),
      mParent(NULL) {
  }
  BoolList(const BoolList& a, Allocator *allocator);
  ~BoolList() { clear(); }

  inline void swap(BoolList &a) {
    Allocator *tmp=mAllocator;
    mAllocator = a.mAllocator;
    a.mAllocator = tmp;

    mList.swap(a.mList);
    mAncestor.swap(a.mAncestor);
  }
  void operator=(const BoolList &a);

  inline operator bool() const { return mList; }
  inline size_t length() const { return mList.length(); }
  inline bool isZero() const { return !mList; }
  inline bool isOne() const { return mList && mList.head()->isZero(); }
  inline const BoolMonom* lm() const { return mList.head(); }
  inline bool isProlong() const { return mList && BoolMonom::compare(lm(), &mAncestor) == 0; }
  int norm() const;
  int degree() const;

  inline ConstIterator begin() const { return mList.begin(); }

  inline void setZero() { mList.clear(); }
  inline void setOne() {
    mList.clear();
    mList.push(new(mAllocator) BoolMonom(mAllocator));
  }

  inline void add(int *var, int n) {
    mList.push(new(mAllocator) BoolMonom(var, n, mAllocator));
  }
  void canonicalize();

  void add(const BoolList& a);
  void mult(const BoolList& a, BoolMonom::Variable var);
  inline void prolong(const BoolList& a, BoolMonom::Variable var) {
    mParent = &a;
    mult(a, var);
    if (mList && lm()->divisible(*a.lm()) && lm()->divisible(var))
      mAncestor = *lm();
  }
  void mult(const BoolList& a, const BoolMonom& m);
  void mult(const BoolList& a, const BoolList& b);
  inline void mult(BoolMonom::Variable var) {
    BoolList tmp(mAllocator);
    tmp.mult(*this, var);
    swap(tmp);
  }
  inline void addmult(BoolMonom::Variable var) {
    BoolList tmp(mAllocator);
    tmp.mult(*this, var);
    add1(tmp);
  }
  inline void addmult(const BoolList& a, BoolMonom::Variable var) {
    BoolList tmp(mAllocator);
    tmp.mult(a, var);
    add1(tmp);
  }
  inline void addmult(const BoolList& a, const BoolMonom& m) {
    BoolList tmp(mAllocator);
    tmp.mult(a, m);
    add1(tmp);
  }

  template <typename D> void nf(const D& a) {
    assert(mList);
    BoolList *f=a.find(mList.head());
    if (f != NULL) {
      if (lm()->degree() == f->lm()->degree() && isProlong() && !f->isProlong())
        f->mAncestor = mAncestor;
      BoolMonom m(mAllocator);
      do {
        assert(mList);
        assert(f);
        m.div(*mList.head(), *f->mList.head());
        if (m.degree() == 0)
          add(*f);
        else if (m.degree() == 1)
          addmult(*f, m.variable(0));
        else
          addmult(*f, m);
        if (mList)
          f = a.find(mList.head());
        else
          break;
      } while(f);
    }
  }
  template <typename D> bool isNfTail(const D& a) const {
    assert(mList);
    List<const BoolMonom*>::ConstIterator i(mList.begin());
    ++i;
    while(i && !a.find(i.data()))
      ++i;
    return !i;
  }
  template <typename D> void nfTail(const D& a) {
    assert(mList);
    BoolMonom m(mAllocator);
    List<const BoolMonom*>::Iterator i(mList.begin());
    ++i;
    while(i) {
      BoolList *f=a.find(i.data());
      if (f == NULL)
        ++i;
      else {
        m.div(*i.data(), *f->mList.head());
        if (m.degree() == 0)
          add(*f);
        else if (m.degree() == 1)
          addmult(*f, m.variable(0));
        else
          addmult(*f, m);
      }
    }
    assert(isNfTail(a));
  }

  static inline int compare(const BoolList* a, const BoolList* b) {
    assert(!a->isZero() && !b->isZero());
    return BoolMonom::compare(a->lm(), b->lm());
  }

  bool assertValid() const;
};

std::ostream& operator<<(std::ostream& out, const BoolList &a);

}

#endif // GINV_BOOL_LIST_H
