/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A.                                  *
 *   BlinkovUA@info.sgu.ru                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <string>

#include "tq.h"

using namespace GInv;

int main(int argc, char *argv[]) {
  if (argc != 2 && argc != 3) {
    std::cout << "ginvsat <cnf-file> [strict-file]" << std::endl;
    return EXIT_FAILURE;
  }

  std::ifstream cnf(argv[1]);

  if (!cnf.is_open()) {
    std::cerr << "Unable to open file '" << argv[1] << "'" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "cnf-file - '" << argv[1] << "'" << std::endl;
  std::string str;
  while(!cnf.eof() && cnf.peek() == 'c') {
    getline(cnf, str);
    std::cout << str << std::endl;
  }

  if (cnf.peek() != 'p') {
    std::cerr << "Error to content file '" << argv[1] << "'" << std::endl;
    return EXIT_FAILURE;
  }
  cnf.ignore(6);
  int pols=-1, vars=-1;
  cnf >> vars >> pols;
  std::cout << "varibles - " << vars << ", polynomials - " << pols << std::endl;
  assert(pols > 0 && vars > 0);
  
  int *table=new int[vars];
  
  if (argc == 3) {
    std::ifstream strict(argv[2]);

    if (!strict.is_open()) {
      std::cerr << "Unable to open file '" << argv[2] << "'" << std::endl;
      return EXIT_FAILURE;
    }
    for(int i=0; i < vars; i++) {
      int v;
      strict >> std::ws >> v;
      std::cout << i << " - " << v << std::endl;
    }
  }

  BoolTQ tq(vars);
  int line=0;
  while(!cnf.eof() && cnf.peek() != '%') {
    int var=0;
    BoolPoly *p=new BoolPoly;
    p->setOne();
    do {
      cnf >> var;
      assert(var <= vars);
      std::cout << var << ' ';
      if (var > 0) {
        p->mult(vars - var);
//         p->mult(var-1);
      }
      else if (var < 0) {
        p->addmult(vars+var);
//         p->addmult(-var-1);
      }
    } while(var);
    tq.insert(p);
    cnf >> std::ws;
    std::cout << *p << std::endl;
    ++line;
  }
  assert(line == pols);
  cnf.close();
  delete[] table;

  tq.build();
  
  if (!tq.isOne() && true) {
    bool test=true;
    std::ifstream cnf(argv[1]);
    std::string str;
    while(!cnf.eof() && cnf.peek() == 'c') {
      getline(cnf, str);
    }
    getline(cnf, str);
    for(int i=0; i < line; i++) {
      int var=0;
      BoolPoly *p=new BoolPoly;
      p->setOne();
      do {
        cnf >> var;
        assert(var <= vars);
        if (var > 0) {
          p->mult(vars - var);
//           p->mult(var-1);
        }
        else if (var < 0) {
          p->addmult(vars+var);
//           p->addmult(-var-1);
        }
      } while(var);
//       std::cerr << *p << std::endl;
      p->nf(tq);
      if (!p->isZero()) {
        test = false;
        std::cerr << "Error evalution on line " << line << std::endl;
        std::cerr << *p << std::endl;
      }
    }
    cnf.close();
    if (test)
      std::cout << "Successful test" << std::endl;
    else
      std::cout << "Test failure" << std::endl;
  }

  return EXIT_SUCCESS;
}
