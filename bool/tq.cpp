/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iomanip>

#include "tq.h"

namespace GInv {

int BoolTQ::lengthQ() const {
  int sum=0;
  S::ConstIterator i(mS->minimum());
  while(i) {
    sum += i.key()->length();
    ++i;
  }
  sum += mQ->length();
  return sum;
}

int BoolTQ::lengthT() const {
  int sum=0;
  for(int i=0; i < mSize; i++)
    sum += mD[i].length();
  return sum;
}

void BoolTQ::clearS(S::ConstIterator i) {
  while(i) {
    if (i.right())
      moveS2Q(i.right());
    Q::ConstIterator j(i.key()->begin());
    assert(j);
    do {
      delete j.data();
      ++j;
    } while(j);
    delete i.key();
    i = i.left();
  }  
}

void BoolTQ::clear() {
  Q::ConstIterator j(mQ->begin());
  while(j) {
    delete j.data();
    ++j;
  }
  mQ->clear();

  clearS(mS->root());
  mS->clear();

  for(int i=0; i < mSize; i++)
    mD[i].clearAll();
}

void BoolTQ::insert(BoolPoly* p) {
  assert(!mIsOne);
  if (p->isZero())
    delete p;
  else if (p->isOne()) {
    delete p;
    mIsOne = true;
    clear();
  }
  else {
    p->reallocate();
    mQ->push(p);
  }
}

BoolPoly* BoolTQ::find(const BoolMonom* m) const {
  BoolPoly *r=NULL;
  if (mInvDiv) {
    if (m->degree()) {
      assert(0 <= m->variable(0) && m->variable(0) < mSize);
      r = mD[m->variable(0)].find(m);
      if (r == NULL)
        for(int v=1; v < m->degree(); v++) {
          assert(0 <= m->variable(v) && m->variable(v) < mSize);
          if (mD[m->variable(v)].isLinear()) {
            r = mD[m->variable(v)].find(m);
            assert(r);
            break;
          }
        }
    }
  }
  else {
    for(int v=0; v < m->degree(); v++) {
      assert(0 <= m->variable(v) && m->variable(v) < mSize);
      r = mD[m->variable(v)].find(m);
      if (r)
        break;
    }
  }
  return r;
}

// int BoolTQ::minDegreeQ() const {
//   int r=-1;
//   Q::ConstIterator j(mQ->begin());
//   while(j) {
//     if (r < 0 || j.data()->lm()->degree() < r)
//       r = j.data()->lm()->degree();
//     ++j;
//   }
//   return r;
// }

inline int BoolTQ::degreeS() const {
  return (mS->root()) ? mS->root().key()->head()->lm()->degree(): -1;
}

void BoolTQ::moveS2Q(S::ConstIterator i) {
  while(i) {
    if (i.right())
      moveS2Q(i.right());
    Q::ConstIterator j(i.key()->begin());
    assert(j);
    do {
      assert(!j.data()->isZero());
      mQ->push(j.data());
      ++j;
    } while(j);
    delete i.key();
    i = i.left();
  }  
}

int BoolTQ::reduceQ() {
  assert(mQ);
  int d=-1;
  Q::Iterator i(mQ->begin());
  while(i) {
    BoolPoly* p=i.data();
    assert(!p->isZero());
    p->nf(*this);
    if (p->isZero()) {
      delete p;
      i.del();
    }
    else if (p->isOne()) {
      mIsOne = true;
      clear();
      return -1;
    }
    else {
      p->reallocate();
      if (d < 0 || d > p->lm()->degree())
	d = p->lm()->degree();
      ++i;
    }
  }
  mQ->reallocate();
  return d;
}
  
void BoolTQ::moveQ2S(int d) {
  assert(mQ);
  assert(mS);
  assert(mS->length() == 0 || d == degreeS());
  Q q;
  Q::Iterator i(mQ->begin());
  while(i) {
    BoolPoly* p=i.data();
    assert(!p->isZero());
    if (p->lm()->degree() != d)
      ++i;
    else {
      assert(p->lm()->degree() == d);
      i.del();
      p->nfTail(*this);
      p->reallocate();
      q.push(p);
      S::ConstIterator f(mS->find(&q));
      if (f)
	f.key()->push(p);
      else {
	Q *a=new Q;
	a->push(p);
	mS->insert(a);
      }
      q.pop();
    }
  }
  mS->reallocate();
  mQ->reallocate();
}

void BoolTQ::reduceHigh() {
  int d=degreeS();
  Q q;
  S::ConstIterator i(mS->maximum());
  while(i) {
    Q::Iterator j(i.key()->begin());
    assert(j);
    assert(j.data()->lm()->degree() == d);
    assert(find(j.data()->lm()) == NULL);
//     j.data()->nfTail(*this);
    int n=j.data()->norm();
//     std::cout << "reduceHigh1 " << i.key()->length() << " n = " << n << " "  << *j.data() << std::endl;
    Q::ConstIterator k(j.get());
    while(j) {
      assert(BoolMonom::compare(j.data()->lm(), j.data()->lm()) == 0);
//       j.data()->nfTail(*this);
      int n1=j.data()->norm();
//       std::cout << "reduceHigh1 " << i.key()->length() << " n = " << n1 << " "  << *j.data() << std::endl;
      if (n1 < n) {
        n = n1;
        Q::ConstIterator tmp(j.get());
        j.insert(k);
        k = tmp;
      }
      ++j;
    }
    
    j = i.key()->begin();
    BoolPoly *p=k.data();
    j.insert(k);
    ++j;
    while(j) {
      j.data()->add(*p);
//       std::cout << "reduceHigh2 " << i.key()->length() << " "  << *j.data() << std::endl;
      if (j.data()->isZero()) {
        delete j.data();
        j.del();
      }
      else if (j.data()->isOne()) {
        mIsOne = true;
        clear();
        return;
      }
      else if (j.data()->lm()->degree() !=  d) {
        assert(j.data()->lm()->degree() <  d);
        j.data()->reallocate();
//         moveS2Q(mS->root());
//         mS->clear();
//         return;
        mQ->push(j.data());
        j.del();
      }
      else {
        j.data()->reallocate();
        q.push(j.data());
        S::ConstIterator f(mS->find(&q));
        if (f)
          f.key()->push(j.data());
        else {
          Q *a=new Q;
          a->push(j.data());
          mS->insert(a);
        }
        q.pop();
        assert(!q);
        j.del();
      }
    }
    assert(i.key()->length() == 1);
    --i;
  }
}

void BoolTQ::reduceLow() {
  
}

void BoolTQ::build() {
  mTimer.start();
  
  mInvDiv = true;
  bool prl = false;
  int degQ=-1, degS=-1;
  int prn=0;
  do {
    assert(mS->length() == 0);
    degQ = reduceQ();
    degS = degreeS();
    if (degS > 0 && degQ > 0 && degS > degQ) {
      moveS2Q(mS->root());
      mS->clear();
    }
    if (degQ > 0 && (degS < 0 || degS == degQ))
      moveQ2S(degQ);
    reduceHigh();
    
    S::ConstIterator f(mS->minimum());
    while(f) {
      BoolPoly *p=f.key()->head();
      assert(find(p->lm()) == NULL);
      int d = p->lm()->degree();

      assert(f.key()->length() == 1);
      delete f.key();
      mS->remove(f);
 
      int i=p->lm()->variable(0);
      bool h=!mD[i];

      p->nfTail(*this);
      for(int j=i; j < mSize; j++) {
        mD[j].remove(p->lm(), *this);
        mD[j].reallocate();
      }
      assert(mD[i].find(p->lm()) == NULL);
      mD[i].insert(p, *this, prl);
      mD[i].nfTail(*this);
      for(int j=i+1; j < mSize; j++) {
        if (mD[j])
          mD[j].nfTail(*this);
      }
      
      ++prn;
      std::cout << prn << " " << *p->lm() << " norm = " << p->norm() 
                << " S = " << mS->length()  << " Q = " << mQ->length() << std::endl;
      if (p->lm()->degree() == 1) {
        ++mElimination;
        std::cout << "elimination = " << mElimination << '(' << mSize << ')' << std::endl;
      }
      ++f;
    }
  } while(mS->length() > 0 || mQ->length() > 0);

  std::cout << std::endl << "**********************************************" << std::endl
            << "elimination = " << mElimination << '(' << mSize << ')'<< std::endl;
  
  do {
//     assert(mS->length() == 0 && mQ->length() == 0);
    degS = degreeS();
    degQ = -1;
    if (mS->length() == 0) {
      do {
        std::cout << "1 reduceQ = " << mQ->length() << std::endl;
        degQ = reduceQ();
        std::cout << "2 reduceQ = " << mQ->length() << std::endl;
        if (degQ > 0) {
          moveQ2S(degQ);
          assert(mS->length() > 0);
          break;
        }
      
        mBorderDegree = -1;
        mBorderVariable = -1;
        for(int i=0; i < mSize; i++) {
          if (mD[i] && !mD[i].isLinear()) {
            mD[i].border(*this);
            for(int j=i+1; j < mSize; j++) {
              if (mD[j] && !mD[j].isLinear())
                mD[i].border(j, *this);
            }
          }
        }
        std::cout << "BorderDegree = " << mBorderDegree << " BorderVariable = " << mBorderVariable << std::endl;        
        if (mBorderDegree < 0)
          break;

        for(int i=0; i < mSize; i++) {
          if (mD[i] && !mD[i].isLinear()) {
            mD[i].borderProlong(*this);
            for(int j=i+1; j < mSize; j++) {
              if (mD[j] && !mD[j].isLinear())
                mD[i].borderProlong(j, *this);
            }
          }
        }
        std::cout << "BorderDegree = " << mBorderDegree << " BorderVariable = " << mBorderVariable
                  << " Q = " << mQ->length() << std::endl;
      } while(true);
    }
    
    if (mQ->length() == 0 && mS->length() == 0)
      break;
    
//     if (degS > degQ) {
//       moveS2Q(mS->root());
//       mS->clear();
//     }
//     if (degS < 0 || degS == degQ)
//       moveQ2S(degQ);

    assert(mS->length() > 0);
    std::cout << "1 reduceHigh = " << mS->length() << std::endl;
    reduceHigh();
    std::cout << "1 reduceHigh = " << mS->length() << std::endl;
    assert(mS->length() > 0);

   
    std::cout << "3 S = " << mS->length()  << " Q = " << mQ->length() << std::endl;
    S::ConstIterator f(mS->minimum());
    while(f) {
      BoolPoly *p=f.key()->head();
      assert(find(p->lm()) == NULL);
      int d = p->lm()->degree();
      
      assert(f.key()->length() == 1);
      delete f.key();
      mS->remove(f);
      
      int i=p->lm()->variable(0);
      bool h=!mD[i];
      
      p->nfTail(*this);
      for(int j=i; j < mSize; j++) {
        mD[j].remove(p->lm(), *this);
        mD[j].reallocate();
      }
      assert(mD[i].find(p->lm()) == NULL);
      mD[i].insert(p, *this, prl);
//       mD[i].nfTail(*this);
//       for(int j=i+1; j < mSize; j++) {
//         if (mD[j])
//           mD[j].nfTail(*this);
//       }
      
      ++prn;
      std::cout << prn << " " << *p->lm() << " norm = " << p->norm() 
      << " S = " << mS->length()  << " Q = " << mQ->length() << std::endl;
      if (p->lm()->degree() == 1) {
        ++mElimination;
        std::cout << "elimination = " << mElimination << '(' << mSize << ')' << std::endl;
      }
      ++f;
    }
    for(int i=0; i < mSize; i++) {
      mD[i].nfTail(*this);
    }
    std::cout << "4 S = " << mS->length()  << " Q = " << mQ->length() << std::endl;
//     return;
  } while(true);
  
  mTimer.stop();

  if (!mIsOne)
    std::cout << "elimination = " << mElimination << '(' << mSize << ')' << std::endl;
  else
    std::cout << "system is 1" << std::endl;
  std::ios::fmtflags flags = std::cout.flags();
  std::cout.flags(flags | std::ios::fixed);
  std::cout
      << " |" << std::endl
      << " |" << std::endl
      << " |" <<  std::setprecision(2) << mTimer.userTime() << std::endl
      << " |" <<  std::setprecision(2) << Allocator::timer().userTime() << std::endl
      << " |" <<  std::setprecision(2) << Allocator::maxMemory()/(1024.*1024.) << std::endl
      << " |" << std::endl
      << " |" << std::endl;
}

}
