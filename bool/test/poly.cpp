/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bool/poly.h"

using namespace GInv;

class TestPoly: public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(TestPoly);
  CPPUNIT_TEST(test1);
  CPPUNIT_TEST(test2);
  CPPUNIT_TEST(test3);
  CPPUNIT_TEST(test4);
  CPPUNIT_TEST_SUITE_END();

  int n;

public:
  void setUp();
  void tearDown();

  void test1();
  void test2();
  void test3();
  void test4();
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestPoly);

void TestPoly::setUp() {
  n = 38;
}

void TestPoly::tearDown() {
}

void TestPoly::test1() {
  BoolPoly a;
  CPPUNIT_ASSERT(!a);
  CPPUNIT_ASSERT(a.length() == 0);
  CPPUNIT_ASSERT(a.isZero());

  a.setOne();
  CPPUNIT_ASSERT(a);
  CPPUNIT_ASSERT(a.length() == 1);
  CPPUNIT_ASSERT(a.isOne());
  CPPUNIT_ASSERT(a.lm()->isZero());

  BoolPoly b(a);
  CPPUNIT_ASSERT(b);
  CPPUNIT_ASSERT(b.length() == 1);
  CPPUNIT_ASSERT(b.isOne());
  CPPUNIT_ASSERT(b.lm()->isZero());

  BoolPoly c(a);
  c = b;
  CPPUNIT_ASSERT(c);
  CPPUNIT_ASSERT(c.length() == 1);
  CPPUNIT_ASSERT(c.isOne());
  CPPUNIT_ASSERT(c.lm()->isZero());
}

void TestPoly::test2() {
  BoolPoly a;
  int m[1];
  a.add(m, 0);
  for(int i=n; i >= 0; i--) {
    m[0] = i;
    a.add(m, 1);
  }
  a.canonicalize();
  CPPUNIT_ASSERT(a.length() == (n+2));

  BoolPoly b;
  b.mult(a, n/2);
  CPPUNIT_ASSERT(b.length() == n);

  BoolPoly c;
  for(int i=n/2; i >= 0; i--) {
    m[0] = i;
    c.add(m, 1);
  }
  c.canonicalize();
  c.mult(c, a);
  CPPUNIT_ASSERT(c.length() == (n-n/2)*(n/2+1));
}

void TestPoly::test3() {
}

void TestPoly::test4() {
}
