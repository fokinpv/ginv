#include <cstdlib>

#include "bool/tq.h"

using namespace GInv;

int main(int argc, char *argv[]) {
  Allocator allocator[1];
  BoolJanet janet(allocator);
  BoolTQ tq(20);
  BoolPoly p;

  p.setOne();
  p.mult(19);
  p.mult(11);
  p.mult(3);
  janet.insert(&p, tq);


  p.setOne();
  p.mult(19);
  p.mult(15);
  p.mult(2);
  janet.insert(&p, tq);

  p.setOne();
  p.mult(19);
  p.mult(12);
  p.mult(3);
  janet.insert(&p, tq);

  p.setOne();
  p.mult(19);
  p.mult(16);
  janet.insert(&p, tq);

  p.setOne();
  p.mult(19);
  p.mult(5);
  janet.insert(&p, tq);

  p.setOne();
  p.mult(18);
  janet.insert(&p, tq);
  
  janet.saveImage("pdf", "janet1.pdf");

  int vars[]={19, 11, 3};
  BoolMonom m(vars, 2, allocator);
  std::cout << m << std::endl;
  janet.remove(&m, tq);


  p.setOne();
  p.mult(19);
  p.mult(4);
  janet.insert(&p, tq);

  janet.saveImage("pdf", "janet2.pdf");

  return EXIT_SUCCESS;
}
