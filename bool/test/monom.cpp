/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bool/monom.h"

using namespace GInv;

class TestMonom: public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(TestMonom);
  CPPUNIT_TEST(test1);
  CPPUNIT_TEST(test2);
  CPPUNIT_TEST(test3);
  CPPUNIT_TEST(test4);
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp();
  void tearDown();

  void test1();
  void test2();
  void test3();
  void test4();
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestMonom);

void TestMonom::setUp() {
}

void TestMonom::tearDown() {
}

void TestMonom::test1() {
  Allocator allocator[1];

  BoolMonom a(allocator);
  CPPUNIT_ASSERT(a.isZero());
  CPPUNIT_ASSERT(a.degree() == 0);

  BoolMonom b(3, allocator);
  CPPUNIT_ASSERT(!b.isZero());
  CPPUNIT_ASSERT(b.degree() == 1);
  CPPUNIT_ASSERT(b.variable(0) == 3);

  BoolMonom c(b, allocator);
  CPPUNIT_ASSERT(!c.isZero());
  CPPUNIT_ASSERT(c.degree() == 1);
  CPPUNIT_ASSERT(c.variable(0) == 3);
}

void TestMonom::test2() {
  {
  Allocator allocator[1];
  BoolMonom x1(1, allocator),
            x2(x1, 2, allocator),
            a(x1, x2, allocator);

  GC<BoolMonom> b;
  for(int i=0; i < 1; i++) {
    b.div(a, 2);
    b.div(b, x1);
  }
  b.reallocate();
  }
  CPPUNIT_ASSERT(Allocator::currMemory() == 0);
}

void TestMonom::test3() {
  Allocator allocator[1];
  BoolMonom a(allocator);

  BoolMonom b(3, allocator);
  CPPUNIT_ASSERT(b.divisible(a));

  BoolMonom c(b, 4, allocator);
  CPPUNIT_ASSERT(c.divisible(b));
  CPPUNIT_ASSERT(!b.divisible(c));
  a.div(c, b);
  CPPUNIT_ASSERT(a.degree() == 1);
  CPPUNIT_ASSERT(a.variable(0) == 4);
}

void TestMonom::test4() {
  Allocator allocator[1];
  BoolMonom a(1, allocator);

  CPPUNIT_ASSERT(a.variable(0) == 1);
  BoolMonom b(a, 4, allocator);
  CPPUNIT_ASSERT(b.variable(0) == 4);
  CPPUNIT_ASSERT(b.variable(1) == 1);
  int m1[3] = {4, 2, 1};
  BoolMonom c(m1, 3, allocator);
  CPPUNIT_ASSERT(c.variable(0) == 4);
  CPPUNIT_ASSERT(c.variable(1) == 2);
  CPPUNIT_ASSERT(c.variable(2) == 1);
  CPPUNIT_ASSERT(b.lex(a) == 1);
/*
  b.mult(2);
  b.mult(4);
  b.mult(1);
  CPPUNIT_ASSERT(a.drl(b) == 0);

  a.mult(3);
  b.mult(4);
  CPPUNIT_ASSERT(a.drl(b) == 1);

  a.mult(a);
  b.mult(b);
  a.mult(b);
  CPPUNIT_ASSERT(a.degree() == 4);

  a.setZero(); b.setZero();
  a.mult(9);a.mult(12);a.mult(18);
  b.mult(9);b.mult(11);b.mult(18);
  CPPUNIT_ASSERT(a.drl(b) == -1);*/
}

