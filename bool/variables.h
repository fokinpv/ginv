/***************************************************************************
 *   Copyright (C) 2013 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_VARIABLES_H
#define GINV_BOOL_VARIABLES_H

#include "util/rbtree.h"
#include "monom.h"


namespace GInv {

class Variables {
  static int compare(BoolMonom::Variable a, BoolMonom::Variable b) {
    if (a > b)
      return 1;
    else if (a < b)
      return -1;
    else
      return 0;
  }

  GC< RBTree<BoolMonom::Variable, compare> > mVariables;

public:
  explicit Variables(): mVariables() {}
  ~Variables() {}

  inline operator bool() const { return mVariables; }
  inline int length() const { return mVariables.length(); }

  inline bool in(BoolMonom::Variable v) const { return mVariables.find(v); }
  inline void add(BoolMonom::Variable v) {
    if (!mVariables.find(v))
      mVariables.insert(v);
  }
  
  friend std::ostream& operator<<(std::ostream& out, const Variables &a);
 
};


}

#endif // GINV_BOOL_VARIABLES_H
