/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GINV_BOOL_TQ_H
#define GINV_BOOL_TQ_H

#include "util/list.h"
#include "util/list.h"

#include "monom.h"
#include "poly.h"
#include "division.h"

namespace GInv {

class BoolTQ {
  typedef GC<BoolDivision>                   D;
  typedef GC<List<BoolPoly*> >               Q;

  static inline int compare(Q* a, Q* b) {
    assert(*a && *b);
    return BoolMonom::compare(a->head()->lm(), b->head()->lm());
  }
  
  typedef GC<RBTree<Q*, compare> >  S;

  int     mSize;
  bool    mIsOne;
  D*      mD;
  Q*      mQ;
  S*      mS;
  int     mElimination;
  Timer   mTimer;
  bool    mInvDiv;

  int lengthQ() const;
  int lengthT() const;

  void clearS(S::ConstIterator i);
  void clear();
  
//   int minDegreeQ() const;
  int degreeS() const;
  
  void moveS2Q(S::ConstIterator i);
  int reduceQ();
  void moveQ2S(int d);
  void reduceHigh();
  void reduceLow();
  
public:
  int mBorderDegree;
  int mBorderVariable;
  
  BoolTQ(int size):
      mSize(size),
      mIsOne(false),
      mD(new D[mSize]),
      mQ(new Q),
      mS(new S),
      mElimination(0),
      mTimer(),
      mInvDiv(true) {
    assert(size > 0);
    BoolDivision::mSize = size;
  }
  ~BoolTQ() { 
    clear(); 
    delete[] mD;
    delete mQ;
    delete mS;
  }

  inline int Break() const { assert(mQ); return mQ->length() > mSize*mBorderDegree; }
  
  inline int size() const { return mSize; }
  inline bool isOne() const { return mIsOne; }
  inline const Timer timer() const { return mTimer; }

  void insert(BoolPoly* p);
  BoolPoly* find(const BoolMonom* m) const;
  void build();
};


}

#endif // GINV_BOOL_TQ_H
