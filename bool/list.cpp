/***************************************************************************
 *   Copyright (C) 2012 by Blinkov Yu. A. and Fokin P. V.                  *
 *   BlinkovUA@info.sgu.ru and fokinpv@gmail.com                           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "util/rbtree.h"

#include "list.h"

namespace GInv {

int BoolList::norm() const {
  return length();
//   int s=0;
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   while(i) {
//     s += i.data()->degree();
//     ++i;
//   }
//   return s;
}

// int BoolList::norm() const {
//   int d=-1;
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   while(i) {
//     if (d < i.data()->degree())
//       d = i.data()->degree();
//     ++i;
//   }
//   return d;
// }

int BoolList::degree() const {
  int d=-1;
  List<const BoolMonom*>::ConstIterator i(mList.begin());
  while(i) {
    if (d < i.data()->degree())
      d = i.data()->degree();
    ++i;
  }
  return d;
}

void BoolList::add1(BoolList& a) {
  assert(this != &a);
  assert(mAllocator == a.mAllocator);
  List<const BoolMonom*>::Iterator i(mList.begin()),
                                   ia(a.mList.begin());
  while(i && ia) {
    int cmp=BoolMonom::compare(i.data(), ia.data());
    if (cmp > 0)
      ++i;
    else if (cmp < 0) {
      i.insert(ia.get());
      ++i;
    }
    else {
      mAllocator->destroy(i.data());
      i.del();
      mAllocator->destroy(ia.data());
      ia.del();
    }
  }
  while(ia) {
    i.insert(ia.get());
    ++i;
  }
  assert(assertValid());
}

BoolList::BoolList(const BoolList& a, Allocator *allocator):
    mAllocator(allocator),
    mList(allocator),
    mAncestor(allocator),
    mParent(a.mParent) {
  List<const BoolMonom*>::Iterator i(mList.begin());
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(ia) {
    i.insert(new(mAllocator) BoolMonom(*ia.data(), mAllocator));
    ++i;
    ++ia;
  }
}

void  BoolList::operator=(const BoolList &a) {
  assert(this != &a);
  clear();
  List<const BoolMonom*>::Iterator i(mList.begin());
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(ia) {
    i.insert(new(mAllocator) BoolMonom(*ia.data(), mAllocator));
    ++i;
    ++ia;
  }
  mAncestor = a.mAncestor;
}

// int BoolList::degree() const {
//   assert(mList);
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   int d=i.data()->degree();
//   ++i;
//   while(i) {
//     if (d < i.data()->degree())
//       d = i.data()->degree();
//     ++i;
//   }
//   return d;
// }

void BoolList::canonicalize() {
  int l=mList.length();
  if (l <= 8) {
    List<const BoolMonom*> tmp(mAllocator);
    List<const BoolMonom*>::Iterator k(tmp.begin());
    while(mList) {
      List<const BoolMonom*>::Iterator i(mList.begin()),
                                       j(i);
      ++i;
      int cmp=1;
      while(i) {
        cmp=BoolMonom::compare(j.data(), i.data());
        if (cmp > 0)
          j = i;
        else if (cmp == 0) {
          mAllocator->destroy(i.data());
          i.del();
          mAllocator->destroy(j.data());
          j.del();
          break;
        }
        ++i;
      }
      if (cmp)
        k.insert(j.get());
    }
    mList.swap(tmp);
    assert(assertValid());
  }
  else {
    int l2=l/2;
    assert(l2 > 1);
    BoolList tmp(mAllocator);
    List<const BoolMonom*>::Iterator i(mList.begin()),
                                     ia(tmp.mList.begin());
    while(l2 > 0) {
      ia.insert(i.get());
      ++ia;
      --l2;
    }
    canonicalize();
    tmp.canonicalize();
    add1(tmp);
  }
}

void BoolList::add(const BoolList& a) {
  assert(this != &a);
  List<const BoolMonom*>::Iterator i(mList.begin());
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(i && ia) {
    int cmp=BoolMonom::compare(i.data(), ia.data());
    if (cmp > 0)
      ++i;
    else if (cmp == 0) {
      mAllocator->destroy(i.data());
      i.del();
      ++ia;
    }
    else if (cmp < 0) {
      i.insert(new(mAllocator) BoolMonom(*ia.data(), mAllocator));
      ++i;
      ++ia;
    }
  }
  while(ia) {
    i.insert(new(mAllocator) BoolMonom(*ia.data(), mAllocator));
    ++i;
    ++ia;
  }
  assert(assertValid());
}

void BoolList::mult(const BoolList& a, BoolMonom::Variable var) {
  assert(this != &a);
  clear();
  List<const BoolMonom*>::Iterator i(mList.begin());
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(ia) {
    i.insert(new(mAllocator) BoolMonom(*ia.data(), var, mAllocator));
    ++ia;
    ++i;
  }
  canonicalize();
  assert(assertValid());
}

void BoolList::mult(const BoolList& a, const BoolMonom& m) {
  assert(this != &a);
  clear();
  List<const BoolMonom*>::Iterator i(mList.begin());
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(ia) {
    i.insert(new(mAllocator) BoolMonom(*ia.data(), m, mAllocator));
    ++ia;
    ++i;
  }
  canonicalize();
  assert(assertValid());
}

void BoolList::mult(const BoolList& a, const BoolList& b) {
  RBTree<const BoolMonom*, BoolMonom::compare> tmp(mAllocator);
  BoolMonom* m=NULL;
  List<const BoolMonom*>::ConstIterator ia(a.mList.begin());
  while(ia) {
    List<const BoolMonom*>::ConstIterator ib(b.mList.begin());
    while(ib) {
      m = new(mAllocator) BoolMonom(*ia.data(), *ib.data(), mAllocator);
      RBTree<const BoolMonom*, BoolMonom::compare>::ConstIterator f(tmp.find(m));
      if (!f)
        tmp.insert(m);
      else {
        tmp.remove(m);
        mAllocator->destroy(f.key());
        mAllocator->destroy(m);
      }
      ++ib;
    }
    ++ia;
  }
  clear();
  RBTree<const BoolMonom*, BoolMonom::compare>::ConstIterator r(tmp.minimum());
  while(r) {
    mList.push(r.key());
    ++r;
  }
  assert(assertValid());
}

// void BoolList::nf(const GINV_BOOL_DIVISION& a) {
//   assert(mList);
//   BoolPoly *f=a.find(mList.head());
//   if (f != NULL) {
//     BoolMonom m(mAllocator);
//     do {
//       assert(mList);
//       assert(f);
//       m.div(*mList.head(), *f->mList.head());
//       if (m.degree() == 0)
//         add(*f);
//       else if (m.degree() == 1)
//         addmult(*f, m.variable(0));
//       else
//         addmult(*f, m);
//       if (mList)
//         f = a.find(mList.head());
//       else
//         break;
//     } while(f);
//   }
// }
//
// bool BoolList::isNfTail(const GINV_BOOL_DIVISION& a) const {
//   assert(mList);
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   ++i;
//   while(i && !a.find(i.data()))
//     ++i;
//   return !i;
// }
//
// void BoolList::nfTail(const GINV_BOOL_DIVISION& a) {
//   assert(mList);
//   BoolMonom m(mAllocator);
//   List<const BoolMonom*>::Iterator i(mList.begin());
//   ++i;
//   while(i) {
//     BoolPoly *f=a.find(i.data());
//     if (f == NULL)
//       ++i;
//     else {
//       m.div(*i.data(), *f->mList.head());
//       if (m.degree() == 0)
//         add(*f);
//       else if (m.degree() == 1)
//         addmult(*f, m.variable(0));
//       else
//         addmult(*f, m);
//     }
//   }
//   assert(isNfTail(a));
// }
//
//
// void BoolList::nf(const GINV_BOOL_DIVISION& a, BoolMonom::Variable var) {
//   assert(mList);
//   BoolPoly *f=a.find(mList.head(), var);
//   if (f != NULL) {
//     BoolMonom m(mAllocator);
//     do {
//       assert(mList);
//       assert(f);
//       m.div(*mList.head(), *f->mList.head());
//       if (m.degree() == 0)
//         add(*f);
//       else if (m.degree() == 1)
//         addmult(*f, m.variable(0));
//       else
//         addmult(*f, m);
//       if (mList)
//         f = a.find(mList.head(), var);
//       else
//         break;
//     } while(f);
//   }
// }
//
// bool BoolList::isNfTail(const GINV_BOOL_DIVISION& a, BoolMonom::Variable var) const {
//   assert(mList);
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   ++i;
//   while(i && !a.find(i.data(), var))
//     ++i;
//   return !i;
// }
//
// void BoolList::nfTail(const GINV_BOOL_DIVISION& a, BoolMonom::Variable var) {
//   assert(mList);
//   BoolMonom m(mAllocator);
//   List<const BoolMonom*>::Iterator i(mList.begin());
//   ++i;
//   while(i) {
//     BoolPoly *f=a.find(i.data(), var);
//     if (f == NULL)
//       ++i;
//     else {
//       m.div(*i.data(), *f->mList.head());
//       if (m.degree() == 0)
//         add(*f);
//       else if (m.degree() == 1)
//         addmult(*f, m.variable(0));
//       else
//         addmult(*f, m);
//     }
//   }
//   assert(isNfTail(a));
// }

// void BoolList::nf(const BoolList& a) {
//   assert(mList && mList.head()->divisible(*a.mList.head()));
//   BoolMonom m(mAllocator);
//   do {
//     m.div(*mList.head(), *a.mList.head());
//     if (m.degree() == 0)
//       add(a);
//     else if (m.degree() == 1)
//       addmult(a, m.variable(0));
//     else
//       addmult(a, m);
//   } while(mList && mList.head()->divisible(*a.mList.head()));
// }
//
// bool BoolList::isNfTail(const BoolList& a) const {
//   assert(mList);
//   List<const BoolMonom*>::ConstIterator i(mList.begin());
//   ++i;
//   while(i && !i.data()->divisible(*a.mList.head()))
//     ++i;
//   return !i;
// }
//
// void BoolList::nfTail(const BoolList& a) {
//   assert(mList);
//   BoolMonom m(mAllocator);
//   List<const BoolMonom*>::Iterator i(mList.begin());
//   ++i;
//   do {
//     while(i && i.data()->divisible(*a.mList.head())) {
//       m.div(*i.data(), *a.mList.head());
//       if (m.degree() == 0)
//         add(a);
//       else if (m.degree() == 1)
//         addmult(a, m.variable(0));
//       else
//         addmult(a, m);
//     }
//     if (i)
//       ++i;
//     else
//       break;
//   } while(true);
//   assert(isNfTail(a));
// }

bool BoolList::assertValid() const {
  if (mList) {
    List<const BoolMonom*>::ConstIterator i(mList.begin());
    assert(i);
    const BoolMonom *prev=i.data();
    ++i;
    while(i) {
      if (BoolMonom::compare(prev, i.data()) <= 0)
        return false;
      prev = i.data();
      ++i;
    }
  }
  return true;
}

std::ostream& operator<<(std::ostream& out, const BoolList &a) {
  if (!a)
    out << '0';
  else {
    BoolList::ConstIterator i(a.begin());
    out << *i.monom();
    ++i;
    while(i) {
      out << " + " << *i.monom();
      ++i;
    }
  }
  return out;
}

}
